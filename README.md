# Stream Utilities

Stream utilities is a website to make streaming easier for a streamer with multiple tools!


# License agreement

This software (Stream Utilities) falls under a no license clause. Using this Software publicly without permission will result into a takedown notice.
Please only use this software for personal development uses and for learning on how to use the different API's in this piece of software.

We thank you for wanting to use Stream utilities for more questions you can always contact Rockster with the information below to talk about giving out a private license.

# Contact information of the owner
Discord : Rockster#0069  
Gitlab : @Rockster  
Email : Contact@rockster.dev  