<?php
include "../smart/config.ini.php";
include "functions/content.php";
checkvalid($conn);
if(isset($_POST['create'])){
    $array = createbanner($conn);
    $bool = $array['bool'];
    if ($bool == "invalid" && $bool != true) {
        echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Warning!</strong><br>' . $array['message'] . '</div>';
    } elseif($bool == true) {
        echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>' . $array['message'] . '</div>';
    } elseif($bool === false) {
        echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error!</strong><br>' . $array['message'] . '</div>';
    }
  }
  if(isset($_POST['update'])){
      $array = updatebanner($conn);
      $bool = $array['bool'];
      if ($bool == "invalid" && $bool != true) {
          echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Warning!</strong><br>' . $array['message'] . '</div>';
      } elseif($bool == true) {
          echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>' . $array['message'] . '</div>';
      } elseif($bool === false) {
          echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error!</strong><br>' . $array['message'] . '</div>';
      }
    }
?>
<html>

<head>
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
  <link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script async src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
  <script src="web-img/js/banner.js"></script>
  <script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script>
  <style>
    .alert {
      padding: 15px;
      background-color: #ee1111;
      color: white;
      opacity: 1;
      transition: opacity 0.6s;
      margin-bottom: 15px;
      width: auto;
      text-align: center;
      border-radius: .25rem;
    }

    #table {
      padding-top: 10px;
    }

    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
    }

    th,
    td {
      text-align: left;
      padding: 16px;
    }

    tr:nth-child(even) {
      background-color: #43495282;
    }
  </style>
</head>

<body>
  <div class="row">
  <form action="" method="post" style="margin-block-end: 0;">
    <div class='column small'><button type="submit" name='submit' class="btn btn-default btn-sm" />
    <span class="fa fa-sign-out-alt"></span> Log out</button></div>
  </form>
    <div class='column small'><button onclick="location.href='dash'" class="btn btn-default btn-sm"><span class="fa fa-home"></span> Go back to Dash</button></div>
  </div>
  <?php
  if(isset($_POST['edit'])){
    include('template/banneredit.php');
  } else {

    $getbanner = getbanner($conn);
    //print_r($getbanner); //Is for debugging
    if(!empty($getbanner)){
      echo "<div id='table'><table><tr><th>Link</th><th>Discord text</th><th>Twitter text</th><th>Instagram text</th><th>Facebook text</th><th>Snapchat text</th><th>Effect</th><th>Effect timer (In ms)</th><th>Refresh timer (In ms)</th><th>Delay (In ms)</th><th>Rainbow mode</th><th></th></tr>";
      foreach($getbanner as $msg) {
      echo $msg['text'];
    }
      echo "</table></div>";
    }
    echo "<div id='formpadding'>";
    include('template/bannercreate.php');
  }
 ?>
  </div>
  </div>
  <?php include 'footer.php';?>
</body>

</html>
