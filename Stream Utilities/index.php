<?php
require_once "../smart/config.ini.php";
require_once 'functions/class.account.php';
require_once 'functions/class.security.php';
$generateUniqueId = false;
$security->checkLoggedin($conn);

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
	<link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
  <title><?php echo $pageTitle;?></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://www.google.com/recaptcha/api.js?render=6LcfObcUAAAAABVww4LbZOXXB83forSKFevJ1W59"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" rel="preload" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script async src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" rel="preload" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" rel="preload" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
  <script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script>
  <script src="/stream/web-img/js/index.js"></script>
<style>
.alert {
	padding: 15px;
	background-color: #ee1111;
	color: white;
	opacity: 1;
	transition: opacity 0.6s;
	margin-bottom: 15px;
	width: 25%;
	margin: auto;
	text-align: center;
	border-radius: .25rem;
}
</style>
</head>
<body>
		<?php
		//Alert boxes are here
		if (isset($_GET['changedurl'])) {
		    echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Warning!<br> </strong> The folder where Stream utilities is in has changed!<br> keep this in mind instead of /admin it now is /stream.<br>Sorry for any inconvenience </div>';
		}
		if(isset($_GET['passreset'])){
			echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>Your password has been reset.</div>';
		}
		if(isset($_GET['removed'])){
			echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>Your account has been removed!.</div>';
		}
		if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {
		        if (isset($_POST['login'])) {
		            $array = $account->login($conn, $recaptcha_secret);
		        } elseif(isset($_POST['register'])) {
		            $array = $account->register($conn, $recaptcha_secret);
		        } elseif(isset($_POST['submit_email']) && $_POST['email']) {
							$array = $account->sendemail($conn, $recaptcha_secret);
						}
						$bool = $array['bool'];
		        if ($bool == "invalid" && $bool != true) {
								echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Warning!</strong><br>' . $array['message'] . '</div>';
		        } elseif($bool == true) {
								echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>' . $array['message'] . '</div>';
		        } else {
								echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error!</strong><br>' . $array['message'] . '</div>';
						}
		    }
		?>

	<div id="login-wrapper">
		<div id="login-head">
			<h4 style="padding-left:16px;">Login</h4>
		</div>
		<div id="login-frame">
			<div id="login">
			<form action="index" method="post">
			  <div class="form-group">
			    <label for="InputUsername2">Username</label>
			    <input autocomplete="off" type="text" class="form-control" id="InputUsername2" placeholder="Enter username" name='username' required>
			  </div>
			  <div class="form-group">
			    <label for="InputPassword3">Password</label>
			    <input type="password" class="form-control" id="InputPassword3" placeholder="Enter password" name='<?= $_SESSION['UniqueId'] ?>' autocomplete="off" required>
					<span toggle="#InputPassword3" class="fa fa-fw fa-eye field-icon toggle-password"></span>
			  </div>
			  <button type="submit" class="btn btn-primary" name="login">Login</button>
			  <span class="btn btn-link" id="password">
	             Forgot Your Password?
	          </span>
	          <div class="form-group">
		          Don't have account? <span class="btn btn-link" id="registerBtn">
		             Sign up here
		          </span>
	      	  </div>
						<input type="hidden" id="recaptchaResponse" name="recaptcha_response">
			</form>
			</div>
		</div>
	</div>
		<div id="register-wrapper" hidden>
			<div id="register-head">
				<h4 style="padding-left:16px;">Register</h4>
			</div>
			<div id="register-frame">
				<div id="register">
				<form action="index" method="post">
				  <div class="form-group">
				    <label for="InputUsername">Username</label>
				    <input type="text" class="form-control" id="InputUsername" placeholder="Enter username" name="username" required>
				  </div>
				  <div class="form-group">
				    <label for="InputEmail">Email</label>
				    <input type="email" class="form-control" id="InputEmail" placeholder="Enter email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
				  </div>
					<div class="form-group">
						<label for="InputEmail2">Confirm Email</label>
						<input type="email" class="form-control" id="InputEmail2" placeholder="Enter email" name="confirmemail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required>
					</div>
				  <div class="form-group">
				    <label for="InputPassword">Password</label>
				    <input type="password" class="form-control" id="InputPassword" placeholder="Enter password" name="<?= $_SESSION['UniqueId'] ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" autocomplete="off" required>
						<span toggle="#InputPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
						<p hidden id="password-strength-text"></p>
					</div>
				  <div class="form-group">
				    <label for="InputPassword2">Confirm Password</label>
				    <input type="password" class="form-control" id="InputPassword2" placeholder="Enter password again" name="confirmpass" autocomplete="off" required>
				  </div>
				  <div class="form-group">
				    <label for="Code">Code</label>
						<?php
						if(isset($_GET['code'])){
							echo '<input type="text" class="form-control" id="InputCode" placeholder="'.$_GET['code'].'" name="code" value="'.$_GET['code'].'" readonly>';
						} else {
							echo '<input type="text" class="form-control" id="InputCode" placeholder="Enter code" name="code" required>';
						}

						 ?>

				  </div>
					<p>By Registering you agree to our <span id="gdprBtn">Privacy Policy</span> and Terms of Service.</p>
				  <button type="submit" class="btn btn-primary" name='register'>Register</button>
		          <div class="form-group">
			          Already have account? <span class="btn btn-link" id="loginbutton">
			             Login here
			          </span>
		      	  </div>
		      	  <input type="hidden" id="recaptchaResponse-1" name="recaptcha_response">
				</form>
				</div>
			</div>
		</div>
			<!-- Password Reset -->
		<div id="password-wrapper" hidden>
			<div id="password-head">
				<h4 style="padding-left:16px;">Reset Password</h4>
			</div>
			<div id="password-frame">
				<div id="password-reset">
				<form method="post">
				  <div class="form-group">
				    <label for="InputEmail3">Please enter your email</label>
				    <input type="email" class="form-control" id="InputEmail3" placeholder="Enter email" name="email" required>
				  </div>
				  <button type="submit" class="btn btn-primary" id="reset-password" name="submit_email">Reset Password</button>
					<div class="form-group">
						Remembered your password? <span class="btn btn-link" id="loginbuttonback">
							 Login here
						</span>
					</div>
		      	  <input type="hidden" id="recaptchaResponse-2" name="recaptcha_response">
				</form>
				</div>
			</div>
		</div>
	<?php
	include "footer.php";
	 ?>
</body>
</html>
