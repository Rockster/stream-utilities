<?php


/*************************/
/*Global security manager*/
/*************************/

  class Security
  {
      public function CodeGenerator()
      {
        return substr(bin2hex(random_bytes(64)), 21, 127);
      }

      public function checkvalid($conn)
      {
          $pagename = basename($_SERVER['REQUEST_URI']);
          if (isset($_SESSION['username'])) {
              $username = $_SESSION['username'];
              $stmt = $conn->prepare("SELECT username, valid FROM users WHERE username = :username");
              $stmt->bindValue(':username', $username);
              $stmt->execute();
              $row = $stmt->fetch(PDO::FETCH_ASSOC);
          }
          if (isset($_POST['submit'])) {
              session_destroy();
              header("location:index");
          }
          if (isset($_SESSION['username']) && $row['valid'] === '1') {
              //user session exists
              //print_r($row);
              return true;
          } elseif ($pagename === "index" || $pagename === "admin" || $pagename === "stream" || strpos($pagename, '?') !== false || $pagename === 'landing') {
              //echo $pagename;
          } else {
              header("location:index");
              //echo $pagename;
          }
      }

      public function checkadmin($conn)
      {
          if (!empty($_SESSION['username'])) {
              $username = $_SESSION['username'];
              $stmt = $conn->prepare("SELECT username, rank FROM users WHERE username = :username");
              $stmt->bindValue(':username', $username);
              $stmt->execute();
              $row = $stmt->fetch(PDO::FETCH_ASSOC);
              if (isset($_SESSION['username']) && $row['rank'] === 'admin') {
                  return true;
              } else {
                  return false;
              }
          } else {
              return false;
          }
          if (isset($_POST['submit'])) {
              session_destroy();
              header("location:index");
          }
      }

      public function checktwitch($conn)
      {
          $stmt = $conn->prepare("SELECT twitchlink FROM users WHERE username = :username AND twitchlink = '0'");
          $stmt->bindValue(':username', $_SESSION['username']);
          $stmt->execute();
          if (!empty($stmt->fetch(PDO::FETCH_ASSOC))) {
              return "<button onclick='location.href=\"linktwitch\"' class='btn btn-default btn-sm'><span class='fab fa-twitch'></span> Link your twitch account!</button>";
          }
      }

      public function recaptcha($recaptcha_secret)
      {
          if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['recaptcha_response'])) {
              //$sendscore var for error logging and debugging
              $sendscore = false;
              // Build POST request:
              $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
              $recaptcha_response = $_POST['recaptcha_response'];
              // Make and decode POST request:
              $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response . '&remoteip=' . $_SERVER["HTTP_CF_CONNECTING_IP"]);
              $recaptcha = json_decode($recaptcha, true);
              //print_r($recaptcha);
              if (!empty($recaptcha['score'])) {
                  $score = $recaptcha['score'];
                  // Take action based on the score returned:
                  if ($score >= 0.3) {
                      return array('bool' => true, 'message' => null);
                  } else {
                      if ($sendscore) {
                          $errMsg = "Recaptcha has recorded a low score!<br> Please don't use an automated system.<br>Debug info : Score = " . $score;
                          return array('bool' => false, 'message' => $errMsg);
                      } else {
                          $errMsg = "Recaptcha has recorded a low score!<br> Please don't use an automated system.";
                          return array('bool' => false, 'message' => $errMsg);
                      }
                  }
              } else {
                  $errMsg = "Recaptcha has failed!<br> Please try again later.";
                  return array('bool' => false, 'message' => $errMsg);
              }
          }
      }
      public function createUniqueId($generateUniqueId) : void
      {
          if (isset($_SESSION['UniqueId'])) {
              //UniqueId Exists in session.
          } elseif ($generateUniqueId) {
              $_SESSION['UniqueId'] = $this->CodeGenerator();
          } else {
              $_SESSION['UniqueId'] = $this->CodeGenerator();
          }
      }

      public function checkLoggedin($conn)
      {
          if (isset($_SESSION['username'])) {
              $username = $_SESSION['username'];
              $stmt = $conn->prepare("SELECT username, valid FROM users WHERE username = :username");
              $stmt->bindValue(':username', $username);
              $stmt->execute();
              $row = $stmt->fetch(PDO::FETCH_ASSOC);
              if ($row['valid'] == '1') {
                  //user session exists
                  header("location:dash");
              }
          }
      }
  }

  $security = new Security();
  $generateUniqueId = false;
  $security->createUniqueId($generateUniqueId);
