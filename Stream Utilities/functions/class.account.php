<?php

require_once 'class.security.php';


/*****************/
/*Account Manager*/
/*****************/

class Account extends Security {

    public function register($conn, $recaptcha_secret) {
        $uniqueid = $_SESSION['UniqueId'];
        $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
        $email = !empty($_POST['email']) ? trim($_POST['email']) : null;
        $confirmemail = !empty($_POST['confirmemail']) ? trim($_POST['confirmemail']) : null;
        $pass = !empty($_POST[$uniqueid]) ? trim($_POST[$uniqueid]) : null;
        $confirmpass = !empty($_POST['confirmpass']) ? trim($_POST['confirmpass']) : null;
        //Code is needed to check if user is using the wildcard for the alpha.
        $code = !empty($_POST['code']) ? trim($_POST['code']) : null;
        //Recaptcha catcher
        $recaptchaarray = $this->recaptcha($recaptcha_secret);
        if ($recaptchaarray['bool']) {
        if ($email === $confirmemail) {
            if ($pass === $confirmpass) {
                $stmt = $conn->prepare("SELECT username, email FROM users WHERE username = :username OR email = :email");
                $stmt->bindValue(':username', $username);
                $stmt->bindValue(':email', $email);
                $stmt->execute();
                //check if code exists.
                $codecheck = $conn->prepare("SELECT verifynumber, type, used FROM wildcards WHERE verifynumber = :code");
                $codecheck->bindParam(':code', $code);
                $codecheck->execute();
                $row = $codecheck->fetch(PDO::FETCH_ASSOC);
                if ($stmt->rowCount() >= 1) {
                    $errMsg = "Username or Email has already been taken.";
                    return array('bool' => false, 'message' => $errMsg);
                    // Return array with False as bool and $errMsg as message

                } elseif ($codecheck->rowCount() < 1) {
                    $errMsg = "The code you used doesn't exist";
                    return array('bool' => false, 'message' => $errMsg);
                    // Return array with False as bool and $errMsg as message

                } elseif ($row['used'] === '1') {
                    $errMsg = "The code you used is already used by someone else.";
                    return array('bool' => false, 'message' => $errMsg);
                    // Return array with False as bool and $errMsg as message
                    $type = $row['type'];
                    if ($type == 'IN') {
                        $valid = '0';
                    } elseif ($type == 'EA') {
                        $valid = '1';
                    }
                    do {
                      $id = $security->codegenerator();
                      $checkid = $conn->prepare("SELECT id FROM users WHERE id = :id");
                      $checkid->bindValue(':id', $id);
                      $checkid->execute();
                    } while($checkid->rowCount() === 1);
                    // Hash the password as we do NOT want to store our passwords in plain text.
                    $passwordHash = password_hash($pass, PASSWORD_ARGON2I, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);
                    // Prepare our INSERT statement.
                    // Remember: We are inserting a new row into our users table.
                    $stmt = $conn->prepare("INSERT INTO users (id, username, password, email, valid) VALUES (:id, :username, :password, :email, :valid);
    	                                    UPDATE wildcards SET usedby = :username, used = '1' WHERE verifynumber = :code");
                    // Bind our variables.
                    $stmt->bindValue(':id', $id);
                    $stmt->bindValue(':username', $username);
                    $stmt->bindValue(':password', $passwordHash);
                    $stmt->bindValue(':email', $email);
                    $stmt->bindValue(':valid', $valid);
                    $stmt->bindValue(':code', $code);
                    // Execute the statement and insert the new account.
                    $result = $stmt->execute();
                    if ($result && $valid === '1') {
                        $regMsg = "You have succesfully completed the register process!<br> Have fun with the early access!";
                        return array('bool' => true, 'message' => $regMsg);
                        // Return array with true as bool and $regMsg as message

                    } elseif ($result && $valid === '0') {
                        $regMsg = "You have successfully complete the register process!<br> You will be notified once you are able to use True amazon one link!";
                        return array('bool' => true, 'message' => $regMsg);
                        // Return array with true as bool and $regMsg as message

                    }
                  } else {
                    $errMsg = "This code has already been used!";
                    return array('bool' => false, 'message' => $errMsg);
                    // Return array with False as bool and $errMsg as message
                  }
            } else {
                $errMsg = "Your password doesn't match!";
                return array('bool' => false, 'message' => $errMsg);
                // Return array with False as bool and $errMsg as message

            }
        } else {
            $errMsg = "Your email doesn't match!";
            return array('bool' => false, 'message' => $errMsg);
            // Return array with False as bool and $errMsg as message

        }
      } else {
          $errMsg = $recaptchaarray['message'];
          return array('bool' => false, 'message' => $errMsg);
      }
    }

    public function login($conn, $recaptcha_secret) {
        //username and password sent from Form
        $uniqueid = $_SESSION['UniqueId'];
        $username = $_POST['username'];
        $password = $_POST[$uniqueid];
        $recaptchaarray = $this->recaptcha($recaptcha_secret);
        if ($username === '') {
            $errMsg = 'You must enter your Username';
            return array('bool' => false, 'message' => $errMsg);
        } elseif ($password === '') {
            $errMsg = 'You must enter your Password';
            return array('bool' => false, 'message' => $errMsg);
        } else {
            $records = $conn->prepare('SELECT id,username,password,valid FROM  users WHERE username = :username OR email =:username');
            $records->bindParam(':username', $username);
            $records->execute();
            $results = $records->fetch(PDO::FETCH_ASSOC);
            if (!empty($results)) {
                if (password_verify($password, $results['password'])) {
                    if ($recaptchaarray['bool']) {
                        if ($results['valid'] != 0) {
                            //Be sure token is always clear after a login to be sure people cannot steal tokens and reset if user hasn't resetted their pass yet
                            //Just generally clean up.
                            $records = $conn->prepare('UPDATE users SET token = "" WHERE username = :username');
                            $records->bindParam(':username', $results['username']);
                            if ($records->execute()) {
                                $_SESSION['username'] = $results['username'];
                                header('location:dash');
                                exit;
                            } else {
                                $errMsg = 'Login failed due to Database issues,<br> Try again later! ';
                                return array('bool' => false, 'message' => $errMsg);
                            }
                        } else {
                            $errMsg = 'Your account is not accepted into the early access program.<br>You will get a confirmation via email if your account is selected for early access! ';
                            return array('bool' => 'invalid', 'message' => $errMsg);
                        }
                    } else {
                        $errMsg = $recaptchaarray['message'];
                        return array('bool' => false, 'message' => $errMsg);
                    }
                } else {
                    $errMsg = 'Password is not correct ';
                    return array('bool' => false, 'message' => $errMsg);
                }
            } else {
                $errMsg = 'Username/Email was not found ';
                return array('bool' => false, 'message' => $errMsg);
            }
        }
    }

    public function resetpassword($conn, $recaptcha_secret) {
        $uniqueid = $_SESSION['UniqueId'];
        $email = $_POST['email'];
        $pass = $_POST[$uniqueid];
        $confirmpass = $_POST['confirmpass'];
        $token = $_POST['token'];
        $recaptchaarray = $this->recaptcha($recaptcha_secret);
        if ($pass === $confirmpass) {
            if ($recaptchaarray['bool']) {
                try {
                    $passwordHash = password_hash($pass, PASSWORD_ARGON2I, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);
                    $records = $conn->prepare('UPDATE users SET password = :password, token = NULL WHERE email = :email AND token = :token');
                    $records->bindValue(':password', $passwordHash);
                    $records->bindParam(':email', $email);
                    $records->bindParam(':token', $token);
                    $records->execute();
                    if (!$records) {
                        return array('text' => "Something went wrong.", 'redirect' => NULL);
                    }
                    return array('text' => NULL, 'redirect' => 'index?passreset');
                }
                catch(PDOException $e) {
                    error_log($e);
                    return false;
                }
            } else {
                return array('text' => $recaptchaarray['message'], 'redirect' => NULL);
            }
        } else {
            return array('text' => "Please retype your password.<br>Be sure your password is the same in both fields!", 'redirect' => NULL);
        }
    }

    public function sendemail($conn, $recaptcha_secret) {
        $pass = "";
        $username = "";
        if(isset($_POST['email'])){
        $recaptchaarray = $this->recaptcha($recaptcha_secret);
        $records = $conn->prepare('SELECT email, password, username FROM users WHERE email=:email');
        $records->bindParam(':email', $_POST['email']);
        $records->execute();
        $row = $records->fetch(PDO::FETCH_ASSOC);
        if ($row['email'] == true) {
            if ($recaptchaarray['bool']) {
                $pass = $row['password'];
                $username = $row['username'];
                $token = $pass;
                $token.= $username;
                $newtoken = password_hash($token, PASSWORD_BCRYPT);
                $records = $conn->prepare('UPDATE users SET token=:token WHERE email=:email');
                $records->bindParam(':email', $_POST['email']);
                $records->bindParam(':token', $newtoken);
                $records->execute();
                $msg = "
    	  	<html>
    	  	<head>
    	  	</head>
    	  	<body>
    	  	<p>Dear " . $username . ",<br>
    	  	With this link you can reset your password!</p><br>
    	  	<a href='https://www.rockster.dev/stream/reset?key=" . $_POST['email'] . "&reset=" . $newtoken . "'>Click here to reset password.</a><br>
          If you didn't request this password reset please ignore this email.<br><br>
          <p>Greetings,</p>
          <p>Rockster.dev</p>
          <br><br><p>This email address is only used for reset services.<br>
          Please sent an email to <a href='mailto:contact@rockster.dev'>Contact@rockster.dev</a> if you need support!</p>
    	  	</body>
    	  	</html>
    	  	";
                $msg = wordwrap($msg, 70, "\r\n");
                // use wordwrap() if lines are longer than 70 characters
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=iso-8859-1';
                $headers[] = 'From: Rockster <no-reply@rockster.dev>';
                $send = @mail($_POST['email'], "Reset password", $msg, implode("\r\n", $headers));
                if ($send) {
                    $errMsg = "The email has been sent! <br>Check your spam if you don't see it.<br> This might take a couple of minutes.";
                    return array('bool' => true, 'message' => $errMsg);
                } else {
                    $errMsg = "Email wasn't send try again later!";
                    return array('bool' => false, 'message' => $errMsg);
                }
            } else {
                $errMsg = $recaptchaarray['message'];
                return array('bool' => false, 'message' => $errMsg);
            }
        } else {
            $errMsg = "Your email doesn't exist!";
            return array('bool' => false, 'message' => $errMsg);
        }
    } else {
      $errMsg = "Please fill in a email!";
      return array('bool' => false, 'message' => $errMsg);
    }
  }
}


  //Class building
  $account = new Account();

?>
