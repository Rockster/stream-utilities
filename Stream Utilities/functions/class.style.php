<?php

$style = new Style();


  /*****************/
  /*UIX, UI Manager*/
  /*****************/
class Style {

  public function getstyle() {
      if (isset($_COOKIE['theme'])) {
          return '<link rel="stylesheet" type="text/css" href="web-img/css/dark.css">';
      } else {
          return '<link rel="stylesheet" type="text/css" href="web-img/css/style.css">';
      }
  }
  public function getspinner() {
    if(isset($_COOKIE['theme'])){
      return '<div id="dvLoading"><img class="imageloader" src="../../web-img/images/loader_white.webp"></div><div id="overlay">';
    } else {
      return '<div id="dvLoading"><img class="imageloader" src="../../web-img/images/loader_black.webp"></div><div id="overlay">';
    }
  }
}
?>
