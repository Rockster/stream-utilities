<?php

  require_once 'class.security.php';

  /******************/
  /*Settings Manager*/
  /******************/

  class Settings extends Security
  {
      public function removeaccount($conn)
      {
          $username = $_SESSION['username'];
          if (isset($_POST['removeaccountyes'])) {
              $stmt = $conn->prepare("DELETE FROM users WHERE username = :username;
          DELETE FROM amazon WHERE affiliate = :username;
          DELETE FROM banner WHERE affiliate = :username;
          UPDATE wildcards SET byuser = :code, usedby = :code  WHERE byuser = :username;");
              $stmt->bindValue(':username', $username);
              $stmt->bindValue(':code', $this->CodeGenerator());
              if ($stmt->execute()) {
                  session_destroy();
                  header('location: index?removed');
              } else {
                  $errMsg = "Something unexpected happened";
                  return array('bool' => false, 'message' => $errMsg);
              }
          } else {
              $errMsg = "Something unexpected happened";
              return array('bool' => false, 'message' => $errMsg);
          }
      }

      public function removetwitchlink($conn)
      {
          $username = $_SESSION['username'];
          if (isset($_POST['removetwitchyes'])) {
              $stmt = $conn->prepare("UPDATE users SET twitchlink = 0, oauthtoken = '', refreshtoken = '', twitch_id = ''  WHERE username = :username ");
              $stmt->bindValue(':username', $username);
              if ($stmt->execute()) {
                  $errMsg = "Twitch has been successfully removed from your profile!";
                  return array('bool' => true, 'message' => $errMsg);
              } else {
                  $errMsg = "Something unexpected happened";
                  return array('bool' => false, 'message' => $errMsg);
              }
          }
      }

      public function showcount($conn)
      {
          $username = $_SESSION['username'];
          if (isset($_POST['changeshowcount']) && !empty($_POST['showcountbool'])) {
              $stmt = $conn->prepare("UPDATE users SET showcount = :showcount WHERE username = :username ");
              $stmt->bindValue(':username', $username);
              $stmt->bindValue(':showcount', $_POST['showcountbool']);
              if ($stmt->execute()) {
                  if ($_POST['showcountbool'] = '0') {
                      $showcount = 'hide counters';
                  } else {
                      $showcount = 'show counters';
                  }
                  $errMsg = "Show viewercount and show chatters has changed to ".$showcount."!";
                  return array('bool' => true, 'message' => $errMsg);
              } else {
                  $errMsg = "Something unexpected happened";
                  return array('bool' => false, 'message' => $errMsg);
              }
          }
      }
  }


  //Class building
  $settings = new Settings();
