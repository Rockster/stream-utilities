<?php

##Class rewrite incoming soon for 0.904-0.91x to split content more ways to make it easier to code later on##


if(session_status() == PHP_SESSION_NONE){
    //session has not started
    session_start();
}
require_once './call.php';
$generateUniqueId = false;
$security->createUniqueId($generateUniqueId);
checkvalid($conn);


/**********************/
/*Verification Manager*/
/**********************/

function checkvalid($conn)
{
    $pagename = checkpage();
    if (isset($_SESSION['username'])) {
        $username = $_SESSION['username'];
        $stmt = $conn->prepare("SELECT username, valid FROM users WHERE username = :username");
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    if (isset($_POST['submit'])) {
        session_destroy();
        header("location:index");
    }
    if (isset($_SESSION['username']) && $row['valid'] === '1') {
        //user session exists
        //print_r($row);
        return true;
    } elseif ($pagename === "index" || $pagename === "admin" || $pagename === "stream" || strpos($pagename, '?') !== false || $pagename === 'landing') {
        //echo $pagename;
    } else {
        header("location:index");
        //echo $pagename;
    }
}

function checkadmin($conn)
{
    if (!empty($_SESSION['username'])) {
        $username = $_SESSION['username'];
        $stmt = $conn->prepare("SELECT username, rank FROM users WHERE username = :username");
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (isset($_SESSION['username']) && $row['rank'] === 'admin') {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
    if (isset($_POST['submit'])) {
        session_destroy();
        header("location:index");
    }
}

function checkpage()
{
    return basename($_SERVER['REQUEST_URI']);
}

/****************/
/*Twitch Manager*/
/****************/

function checktwitch($conn)
{
    $stmt = $conn->prepare("SELECT twitchlink FROM users WHERE username = :username AND twitchlink = '0'");
    $stmt->bindValue(':username', $_SESSION['username']);
    $stmt->execute();
    if (!empty($stmt->fetch(PDO::FETCH_ASSOC))) {
        return "<button onclick='location.href=\"linktwitch\"' class='btn btn-default btn-sm'><span class='fab fa-twitch'></span> Link your twitch account!</button>";
    }
}

function refreshtoken($conn, $client_id_admin, $client_secret_admin)
{
    $stmt = $conn->prepare("SELECT refreshtoken FROM users WHERE username = :username AND twitchlink = '1'");
    $stmt->bindValue(':username', $_SESSION['username']);
    $stmt->execute();
    $array = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!empty($array)) {
        $refresh_token = $array['refreshtoken'];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://id.twitch.tv/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['client_id' => $client_id_admin, 'client_secret' => $client_secret_admin, 'grant_type' => 'refresh_token', 'refresh_token' => $refresh_token]));
        //Execute cURL to refresh token then put array in $oauth
        $output = curl_exec($ch);
        curl_close($ch);
        $oauth = json_decode($output, true);
        //print_r($oauth);
        $oauthtoken = $oauth['access_token'];
        //Update users Oauth token for a fresh new token to use in Statviewer so there are more Requests in the bucket
        $stmt = $conn->prepare("UPDATE users SET oauthtoken = :oauthtoken WHERE username = :username AND refreshtoken = :refreshtoken");
        $stmt->bindValue(':username', $_SESSION['username']);
        $stmt->bindValue(':refreshtoken', $refresh_token);
        $stmt->bindValue(':oauthtoken', $oauthtoken);
        $stmt->execute();
        if ($stmt) {
            return;
        } else {
            $error = "Oauth Key refresh failed<br> Don't worry about it this can happen just refresh";
            return array('exception' => $error);
        }
    }
}

/****************/
/*Amazon Manager*/
/****************/

function createamazon($usa, $gbr, $deu, $can, $esp, $jpn, $fra, $conn)
{
    $username = $_SESSION['username'];
    try {
        $stmt = $conn->prepare("INSERT INTO amazon (affiliate, usa, gbr, deu, can, esp, jpn, fra) VALUES (:affiliate, :usa, :gbr, :deu, :can, :esp, :jpn, :fra)");
        $stmt->bindValue(':affiliate', $username);
        $stmt->bindValue(':usa', $usa);
        $stmt->bindValue(':gbr', $gbr);
        $stmt->bindValue(':deu', $deu);
        $stmt->bindValue(':can', $can);
        $stmt->bindValue(':esp', $esp);
        $stmt->bindValue(':jpn', $jpn);
        $stmt->bindValue(':fra', $fra);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        error_log($e);
    }
}

function checkamazon($conn)
{
    $username = $_SESSION['username'];
    try {
        $stmt = $conn->prepare("SELECT affiliate, gbr, deu, can, esp, jpn, fra FROM amazon WHERE affiliate = :username");
        $stmt->bindValue(':username', $username);
        $stmt->execute();
        $array = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!empty($array)) {
            $amazon = '<div class="column small"><button onclick="location.href=\'amazon\'" class="btn btn-default btn-sm"><span class="fa fa-edit"></span> Edit your true one link</button></div>';
            return array('button' => $amazon);
        } else {
            $amazon = '<div class="column small"><button onclick="location.href=\'amazon\'" class="btn btn-default btn-sm"><span class="fa fa-plus-square"></span> Create your true one link</button></div>';
            return array('button' => $amazon);
        }
    } catch (PDOException $e) {
        error_log($e);
        $exception = "An exception has occured! Please contact Rockster.";
        return array('exception' => $exception);
    }
}

function checkonelink($conn, $country)
{
    $username = $_SESSION['username'];
    $stmt = $conn->prepare("SELECT affiliate, usa, gbr, deu, can, esp, jpn, fra FROM amazon WHERE affiliate = :username ");
    $stmt->bindValue(':username', $username);
    $stmt->execute();
    $array = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!empty($array)) {
        if (!empty($country)) {
            if (!empty($array[$country])) {
                $newtype = "'" . $array[$country] . "' ";
                return $newtype;
            }
        }
    }
}
function updateamazon($gbr, $deu, $can, $esp, $jpn, $fra, $conn)
{
    $username = $_SESSION['username'];
    try {
        $stmt = $conn->prepare("UPDATE amazon SET gbr = :gbr, deu = :deu, can = :can, esp = :esp, jpn = :jpn, fra = :fra WHERE affiliate = :username ");
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':gbr', $gbr);
        $stmt->bindValue(':deu', $deu);
        $stmt->bindValue(':can', $can);
        $stmt->bindValue(':esp', $esp);
        $stmt->bindValue(':jpn', $jpn);
        $stmt->bindValue(':fra', $fra);
        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $e) {
        error_log($e);
    }
}
function checkamazonlink($conn)
{
    $username = $_SESSION['username'];
    $records = $conn->prepare('SELECT COUNT(*) FROM amazon WHERE affiliate=:affiliate');
    $records->bindParam(':affiliate', $username);
    $records->execute();
    if ($records->fetchColumn() != 1) {
        return array('text' => 'template/amazoncreate.php');
    } else {
        return array('text' => 'template/amazonupdate.php');
    }
}

/*****************/
/*Banners Manager*/
/*****************/

function createbanner($conn)
{
    if (!empty($_POST['create'])) {
        if (isset($_POST['twitterbool']) || isset($_POST['discordbool']) || isset($_POST['instagrambool']) || isset($_POST['facebookbool']) || isset($_POST['snapchatbool'])) {
            if (isset($_POST['effect']) == "fade" || isset($_POST['effect']) == "slide" || isset($_POST['effect']) == "direct") {
                $username = $_SESSION['username'];
                $records = $conn->prepare('SELECT COUNT(*) FROM banner WHERE affiliate=:affiliate');
                $records->bindParam(':affiliate', $username);
                $records->execute();
                if ($records->fetchColumn() <= 5) {
                    $code = $_POST['create'];
                    $records = $conn->prepare('SELECT COUNT(*) FROM banner WHERE code=:code');
                    $records->bindParam(':code', $code);
                    $records->execute();
                    if ($records->fetchColumn() != 1) {
                        if (isset($_POST['twitterbool'])) {
                            $twitterbool = "1";
                            $twittername = trim($_POST['twittername'], "@");
                        } else {
                            $twitterbool = "0";
                            $twittername = "";
                        }
                        if (isset($_POST['discordbool'])) {
                            $discordbool = "1";
                            $discordname = trim($_POST['discordname'], "@");
                        } else {
                            $discordbool = "0";
                            $discordname = "";
                        }
                        if (isset($_POST['facebookbool'])) {
                            $facebookbool = "1";
                            $facebookname = trim($_POST['facebookname'], "@");
                        } else {
                            $facebookbool = "0";
                            $facebookname = "";
                        }
                        if (isset($_POST['instagrambool'])) {
                            $instagrambool = "1";
                            $instagramname = trim($_POST['instagramname'], "@");
                        } else {
                            $instagrambool = "0";
                            $instagramname = "";
                        }
                        if (isset($_POST['snapchatbool'])) {
                            $snapchatbool = "1";
                            $snapchatname = trim($_POST['snapchatname'], "@");
                        } else {
                            $snapchatbool = "0";
                            $snapchatname = "";
                        }
                        if (isset($_POST['rainbowbool'])) {
                            $rainbowbool = '1';
                        } else {
                            $rainbowbool = '0';
                        }
                        if (!empty($discordname) || !empty($twittername) || !empty($facebookname) || !empty($instagramname) || !empty($snapchatname)) {
                            $effect = $_POST['effect'];
                            $stmt = $conn->prepare("INSERT INTO banner (affiliate, code, twitter, discord, instagram, facebook, snapchat, discordtext, twittertext, instagramtext, facebooktext, snapchattext, effect, effecttimer, refreshtimer, delay, rainbow) VALUES (:username, :code, :twitter, :discord, :instagram, :facebook, :snapchat, :discordtext, :twittertext, :instagramtext, :facebooktext, :snapchattext, :effect, :effecttimer, :refreshtimer, :delay, :rainbow)");
                            $stmt->bindValue(':username', $username);
                            $stmt->bindValue(':code', $code);
                            $stmt->bindValue(':twitter', $twitterbool);
                            $stmt->bindValue(':discord', $discordbool);
                            $stmt->bindValue(':instagram', $instagrambool);
                            $stmt->bindValue(':facebook', $facebookbool);
                            $stmt->bindValue(':snapchat', $snapchatbool);
                            $stmt->bindValue(':twittertext', $twittername);
                            $stmt->bindValue(':discordtext', $discordname);
                            $stmt->bindValue(':instagramtext', $instagramname);
                            $stmt->bindValue(':facebooktext', $facebookname);
                            $stmt->bindValue(':snapchattext', $snapchatname);
                            $stmt->bindValue(':effect', $effect);
                            $stmt->bindValue(':effecttimer', $_POST['cycle']);
                            $stmt->bindValue(':refreshtimer', $_POST['refreshtimer']);
                            $stmt->bindValue(':delay', $_POST['delaytimer']);
                            $stmt->bindValue(':rainbow', $rainbowbool);
                            if ($stmt->execute()) {
                                $errMsg = "Banner has been created! <br> Your unique link is <a href='https://rockster.dev/banner/v1/" . $username . "/" . $code . "'>https://rockster.dev/banner/v1/" . $username . "/" . $code . "</a>";
                                return array('bool' => true, 'message' => $errMsg);
                            } else {
                                $errMsg = "Something went wrong.";
                                return array('bool' => false, 'message' => $errMsg);
                            }
                        } else {
                            $errMsg = "Your name can't just be @";
                            return array('bool' => false, 'message' => $errMsg);
                        }
                    } else {
                        // do nothing since the banner has already been made with that code this is to prevent new banners to be accidentally made by refreshing
                    }
                } else {
                    $errMsg = "You aren't allowed to have more than 5 banners at the same time.";
                    return array('bool' => false, 'message' => $errMsg);
                }
            } else {
                $errMsg = "The selected effect doesn't exist";
                return array('bool' => false, 'message' => $errMsg);
            }
        } else {
            return array('bool' => false, 'message' => "");
        }
    }
}

function getbanner($conn)
{
    $username = $_SESSION['username'];
    $array = array();
    try {
        $records = $conn->prepare('SELECT affiliate, code, discordtext, twittertext, instagramtext, facebooktext, snapchattext, effect, effecttimer, refreshtimer, delay, rainbow FROM banner WHERE affiliate=:user');
        $records->bindParam(':user', $username);
        $records->execute();
        while ($row = $records->fetch(PDO::FETCH_ASSOC)) {
            if ($row['effect'] == 'direct') {
                $row['effect'] = "Pop";
            }
            if ($row['rainbow'] == 1) {
                $row['rainbow'] = 'Enabled';
            } else {
                $row['rainbow'] = 'Disabled';
            }
            if (!empty($row['code'])) {
                $row['effect'] = ucfirst($row['effect']);
                $text = "<tr><td><a href=https://rockster.dev/banner/v1/" . $username . "/" . $row['code'] . " target='_blank'>https://rockster.dev/banner/v1/" . $username . "/" . $row['code'] . "</a></td>
                <td>" . $row['discordtext'] . "</td><td>" . $row['twittertext'] . "</td>
                <td>" . $row['instagramtext'] . "</td><td>" . $row['facebooktext'] . "</td>
                <td>" . $row['snapchattext'] . "</td><td>" . $row['effect'] . "</td>
                <td>" . $row['effecttimer'] . "</td><td>" . $row['refreshtimer'] . "</td>
                <td>" . $row['delay'] . "</td><td>".$row['rainbow']."</td>
                <td><form action='' method='post' style='margin-block-end: 0;'><button type='submit' name='edit' class='btn btn-default btn-sm' value=" . $row['code'] . " style='float: left;margin-right: 20px;'/><span class='fa fa-pencil-square-o'></span> Edit</button></form></tr>";
                $array[] = array("text" => $text);
            } else {
                return false;
            }
        }
        return $array;
    } catch (PDOException $e) {
        error_log($e);
        return false;
    }
}

function checkbanner($conn, $value)
{
    if (!empty($_POST['edit'])) {
        $username = $_SESSION['username'];
        $stmt = $conn->prepare('SELECT affiliate, code, twitter, discord, instagram, facebook, snapchat, discordtext, twittertext, instagramtext, facebooktext, snapchattext, effect, effecttimer, refreshtimer, delay, rainbow FROM banner WHERE affiliate = :username AND code = :code');
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':code', $_POST['edit']);
        $stmt->execute();
        $array = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!empty($array)) {
            if (!empty($array[$value])) {
                return $array[$value];
            }
        }
    } else {
        return false;
    }
}

function updatebanner($conn)
{
    $username = $_SESSION['username'];
    if (isset($_POST['twitterbool']) || isset($_POST['discordbool']) || isset($_POST['instagrambool']) || isset($_POST['facebookbool']) || isset($_POST['snapchatbool'])) {
        if (isset($_POST['effect']) == "fade" || isset($_POST['effect']) == "slide" || isset($_POST['effect']) == "direct") {
            if (isset($_POST['twitterbool'])) {
                $twitterbool = "1";
                $twittername = trim($_POST['twittername'], "@");
            } else {
                $twitterbool = "0";
                $twittername = "";
            }
            if (isset($_POST['discordbool'])) {
                $discordbool = "1";
                $discordname = trim($_POST['discordname'], "@");
            } else {
                $discordbool = "0";
                $discordname = "";
            }
            if (isset($_POST['facebookbool'])) {
                $facebookbool = "1";
                $facebookname = trim($_POST['facebookname'], "@");
            } else {
                $facebookbool = "0";
                $facebookname = "";
            }
            if (isset($_POST['instagrambool'])) {
                $instagrambool = "1";
                $instagramname = trim($_POST['instagramname'], "@");
            } else {
                $instagrambool = "0";
                $instagramname = "";
            }
            if (isset($_POST['snapchatbool'])) {
                $snapchatbool = "1";
                $snapchatname = trim($_POST['snapchatname'], "@");
            } else {
                $snapchatbool = "0";
                $snapchatname = "";
            }
            if (isset($_POST['rainbowbool'])) {
                $rainbowbool = '1';
            } else {
                $rainbowbool = '0';
            }
            if (!empty($discordname) || !empty($twittername) || !empty($facebookname) || !empty($instagramname) || !empty($snapchatname)) {
                try {
                    $stmt = $conn->prepare("UPDATE banner SET twitter = :twitter, discord = :discord, instagram = :instagram, facebook = :facebook, snapchat = :snapchat, discordtext = :discordtext, twittertext = :twittertext, instagramtext = :instagramtext, facebooktext = :facebooktext, snapchattext = :snapchattext, effect = :effect, effecttimer = :effecttimer, refreshtimer = :refreshtimer, delay = :delay, rainbow = :rainbow WHERE affiliate = :username AND code = :code");
                    $stmt->bindValue(':username', $username);
                    $stmt->bindValue(':code', $_POST['update']);
                    $stmt->bindValue(':twitter', $twitterbool);
                    $stmt->bindValue(':discord', $discordbool);
                    $stmt->bindValue(':instagram', $instagrambool);
                    $stmt->bindValue(':facebook', $facebookbool);
                    $stmt->bindValue(':snapchat', $snapchatbool);
                    $stmt->bindValue(':twittertext', $twittername);
                    $stmt->bindValue(':discordtext', $discordname);
                    $stmt->bindValue(':instagramtext', $instagramname);
                    $stmt->bindValue(':facebooktext', $facebookname);
                    $stmt->bindValue(':snapchattext', $snapchatname);
                    $stmt->bindValue(':effect', $_POST['effect']);
                    $stmt->bindValue(':effecttimer', $_POST['cycle']);
                    $stmt->bindValue(':refreshtimer', $_POST['refreshtimer']);
                    $stmt->bindValue(':delay', $_POST['delaytimer']);
                    $stmt->bindValue(':rainbow', $rainbowbool);
                    if ($stmt->execute()) {
                        $errMsg = "Banner has been updated!";
                        return array('bool' => true, 'message' => $errMsg);
                    } else {
                        $errMsg = "Something went wrong";
                        return array('bool' => false, 'message' => $errMsg);
                    }
                } catch (PDOException $e) {
                    error_log($e);
                }
            } else {
                $errMsg = "Your name can't just be @";
                return array('bool' => false, 'message' => $errMsg);
            }
        } else {
            $errMsg = "The selected effect doesn't exist";
            return array('bool' => false, 'message' => $errMsg);
        }
    } else {
        return array('bool' => false, 'message' => "");
    }
}

/*****************/
/*Invital Manager*/
/*****************/

function createcode($conn)
{
    $security = new Security();
    $uniqueid = $_SESSION['UniqueId'];
    $date = date('Y-m-d H:i:s');
    $username = $_SESSION['username'];
    $code = $security->CodeGenerator();
    $rand_number = random_int(100000, 999999);
    $wildcard = $rand_number . "-" . $code;
    if (!checkadmin($conn)) {
        if (checkvalid($conn)) {
            $records = $conn->prepare("SELECT count(*) FROM wildcards WHERE byuser = :byuser");
            $records->bindValue(':byuser', $username);
            $records->execute();
            if ($records->fetchColumn() >= 5) {
                return array('code' => null, 'bool' => 'TooMany', 'message' => 'You can only have 5 codes at the moment.');
            }
        }
    }
    if (isset($_POST[$uniqueid])) {
        if (!empty($_POST[$uniqueid])) {
            $wildcard = $_POST[$uniqueid];
            if (checkadmin($conn)) {
                $type = "EA";
                $records = $conn->prepare("SELECT count(*) FROM wildcards WHERE verifynumber = :wildcard");
                $records->bindValue(':wildcard', $wildcard);
                $records->execute();
                if ($records->fetchColumn() < 1) {
                    $stmt = $conn->prepare("INSERT INTO wildcards (verifynumber, type, byuser, created) VALUES (:wildcard, :type, :byuser, :created)");
                    // Bind our variables.
                    $stmt->bindValue(':byuser', $username);
                    $stmt->bindValue(':type', $type);
                    $stmt->bindValue(':wildcard', $wildcard);
                    $stmt->bindParam(':created', $date, PDO::PARAM_STR);
                    $stmt->execute();
                    if ($stmt) {
                        $security->createUniqueId($generateUniqueId = true);
                        $wildcard = $rand_number . "-" . $code;
                        $errormsg = "Code has been created.";
                        return array('code' => $wildcard, 'bool' => true, 'message' => $errormsg);
                    } else {
                        $errormsg = "Something went wrong. Or you already have a unique link!";
                        return array('code' => null, 'bool' => false, 'message' => $errormsg);
                    }
                } else {
                    $wildcard = $rand_number . "-" . $code;
                    $errormsg = "Please don't refresh<br> To create a new link scroll down!";
                    return array('code' => $wildcard, 'bool' => false, 'message' => $errormsg);
                }
            } elseif (checkvalid($conn)) {
                $type = "EA";
                $records = $conn->prepare("SELECT count(*) FROM wildcards WHERE byuser = :byuser");
                $records->bindValue(':byuser', $username);
                $records->execute();
                $check = $conn->prepare("SELECT count(*) FROM wildcards WHERE verifynumber = :wildcard");
                $check->bindValue(':wildcard', $wildcard);
                $check->execute();
                if ($records->fetchColumn() <= 5) {
                    if ($records->fetchColumn() < 1) {
                        $stmt = $conn->prepare("INSERT INTO wildcards (verifynumber, type, byuser, created) VALUES (:wildcard, :type, :byuser, :created)");
                        $stmt->bindValue(':byuser', $username);
                        $stmt->bindValue(':type', $type);
                        $stmt->bindValue(':wildcard', $wildcard);
                        $stmt->bindParam(':created', $date, PDO::PARAM_STR);
                        $stmt->execute();
                        if ($stmt) {
                            $security->createUniqueId($generateUniqueId = true);
                            $wildcard = $rand_number . "-" . $code;
                            $errormsg = "Code has been created.";
                            return array('code' => $wildcard, 'bool' => true, 'message' => $errormsg);
                        } else {
                            $errormsg = "Something went wrong.";
                            return array('code' => null, 'bool' => false, 'message' => $errormsg);
                        }
                    } else {
                        $wildcard = $rand_number . "-" . $code;
                        $errormsg = "Please don't refresh<br> To create a new link scroll down!";
                        return array('code' => $wildcard, 'bool' => false, 'message' => $errormsg);
                    }
                } else {
                    $errormsg = "You can only have 5 codes at the moment.";
                    return array('code' => null, 'bool' => false, 'message' => $errormsg);
                }
            } else {
                $errormsg = "dash?notallowed";
                return array('code' => null, 'bool' => false, 'redirect' => $errormsg);
            }
        } else {
            $wildcard = $rand_number . "-" . $code;
            return array('code' => $wildcard, 'bool' => false);
        }
    } else {
        $wildcard = $rand_number . "-" . $code;
        return array('code' => $wildcard, 'bool' => false);
    }
}

function getcode($conn)
{
    $username = $_SESSION['username'];
    $array = array();
    try {
        $records = $conn->prepare('SELECT verifynumber, type, byuser, used, usedby FROM wildcards WHERE byuser=:user');
        $records->bindParam(':user', $username);
        $records->execute();
        while ($row = $records->fetch(PDO::FETCH_ASSOC)) {
            if ($row['used'] == '1') {
                $row['used'] = "Yes";
            } else {
                $row['used'] = "No";
            }
            if (empty($row['usedby'])) {
                $row['usedby'] = "-";
            }
            if ($row['type'] == 'EA') {
                $row['type'] = 'Early access';
            } elseif ($row['type'] == 'IN') {
                $row['type'] = 'Invite code';
            }
            if (!empty($row['verifynumber'])) {
                $row['used'] = ucfirst($row['used']);
                $text = "<tr><td><a href='https://rockster.dev/stream/?code=" . $row['verifynumber'] . "'>" . $row['verifynumber'] . "</a></td><td>" . $row['type'] . "</td><td>" . $row['used'] . "</td><td>" . $row['usedby'] . "</td></tr>";
                $array[] = array("text" => $text);
            } else {
                return false;
            }
        }
        return $array;
    } catch (PDOException $e) {
        error_log($e);
        return false;
    }
}

/*************/
/*Bot Manager*/
/*************/

function createbot($conn)
{
    $security = new Security();
    $uniqueid = $_SESSION['UniqueId'];
    $date = date('Y-m-d H:i:s');
    $username = $_SESSION['username'];
    $code = $security->CodeGenerator();
    $rand_number = random_int(100000, 999999);
    $wildcard = "Oauth:".$rand_number . "-" . $code;
    if (checkadmin($conn)) {
        if (isset($_POST[$uniqueid])) {
            if (!empty($_POST[$uniqueid])) {
                $getid = $conn->prepare("SELECT id FROM users WHERE username = :username");
                $getid->bindValue(':username', $username);
                $getid->execute();
                $row = $getid->fetch(PDO::FETCH_ASSOC);
                $oauth = $_POST[$uniqueid];
                $records = $conn->prepare("SELECT count(*) FROM botsettings WHERE oauth = :wildcard OR client_id = :user_id");
                $records->bindValue(':wildcard', $oauth);
                $records->bindValue(':user_id', $row['id']);
                $records->execute();
                if ($records->fetchColumn() < 1) {
                    $stmt = $conn->prepare("INSERT INTO botsettings (oauth, client_id, created) VALUES (:oauth, :user_id, :created)");
                    $stmt->bindValue(':user_id', $row['id']);
                    $stmt->bindValue(':oauth', $oauth);
                    $stmt->bindParam(':created', $date, PDO::PARAM_STR);
                    $stmt->execute();
                    if ($stmt) {
                        $security->createUniqueId($generateUniqueId = true);
                        $errormsg = "Oauth has been created.";
                        return array('code' => $wildcard, 'bool' => true, 'message' => $errormsg);
                    } else {
                        $errormsg = "Something went wrong. Or you already have a unique link!";
                        return array('code' => null, 'bool' => false, 'message' => $errormsg);
                    }
                } else {
                    $errormsg = "You aren't allowed to make more then 1 Oauth.";
                    return array('code' => $wildcard, 'bool' => false, 'message' => $errormsg);
                }
            } else {
                $errormsg = "dash?notallowed";
                return array('code' => null, 'bool' => false, 'redirect' => $errormsg);
            }
        } else {
            $wildcard = $rand_number . "-" . $code;
            return array('code' => $wildcard, 'bool' => false);
        }
    } else {
        $errormsg = "dash?notallowed";
        return array('code' => null, 'bool' => false, 'redirect' => $errormsg);
    }
}

function getbot($conn)
{
    $username = $_SESSION['username'];
    $array = array();
    try {
        $getid = $conn->prepare("SELECT id FROM users WHERE username = :username");
        $getid->bindValue(':username', $username);
        $getid->execute();
        $rowid = $getid->fetch(PDO::FETCH_ASSOC);
        $records = $conn->prepare('SELECT oauth FROM botsettings WHERE client_id=:user');
        $records->bindParam(':user', $rowid['id']);
        $records->execute();
        while ($row = $records->fetch(PDO::FETCH_ASSOC)) {
            $text = "<tr><td>".$row['oauth']."</td></tr>";
            $array[] = array("text" => $text);
        }
    } catch (PDOException $e) {
        error_log($e);
        return false;
    }
    if (!empty($array)) {
        return $array;
    } else {
        return false;
    }
}
