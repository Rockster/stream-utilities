<?php
function statviewer($conn,$type,$client_id_admin)
{
  $stmt = $conn->prepare("SELECT twitch_id,oauthtoken FROM users WHERE username = :username");
  $stmt->bindValue(':username', $_SESSION['username']);
  $stmt->execute();
  $array = $stmt->fetch(PDO::FETCH_ASSOC);
  $channel_id = $array['twitch_id'];
  $oauthtoken = $array['oauthtoken'];
  if (!empty($channel_id) && !empty($oauthtoken)) {
    //Sanitize URL to make it easier for cURL and easier to read
    $url = "https://api.twitch.tv/helix/users?id=" . $channel_id . "";
    $ch  = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


    $headers   = array();
    $headers[] = "Client-Id: " . $client_id_admin;
    $headers[] = "Authorization: Bearer " . $oauthtoken;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        return 'Error:' . curl_error($ch);
    }
    $json = json_decode($result, true);

    //Array debug
    //print_r($json);

    if (count($json, 1) != 0) {
        $temp['views'] = $json["data"][0]["view_count"];
        curl_close($ch);
        //Lets do it all over for Total Followers

        $url = "https://api.twitch.tv/helix/users/follows/?to_id=" . $channel_id . "";
        $ch  = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers   = array();
        $headers[] = "Client-Id: " . $client_id_admin;
        $headers[] = "Authorization: Bearer " . $oauthtoken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
        }
        $json = json_decode($result, true);

        //Array debug
        //print_r($json);
        $temp['followers'] = $json["total"];

        //Just checking if $temp["$type"] has numbers in it if yes format it per thousand (1,000 10,000 etc) else do nothing to $temp["$type"]
        $newtemp = number_format($temp["$type"]);
        return $newtemp;
    } else {
        return FALSE;
    }
  } else {
    return FALSE;
  }

}

function getname($conn,$client_id_admin){
  $stmt = $conn->prepare("SELECT twitch_id,oauthtoken FROM users WHERE username = :username");
  $stmt->bindValue(':username', $_SESSION['username']);
  $stmt->execute();
  $array = $stmt->fetch(PDO::FETCH_ASSOC);
  $channel_id = $array['twitch_id'];
  $oauthtoken = $array['oauthtoken'];
  if (!empty($channel_id)) {
  //Sanitize URL to make it easier for cURL and easier to read
  $url = "https://api.twitch.tv/helix/users?id=" . $channel_id . "";
  $ch  = curl_init();

  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


  $headers   = array();
  $headers[] = "Client-Id: " . $client_id_admin;
  $headers[] = "Authorization: Bearer " . $oauthtoken;
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

  $result = curl_exec($ch);
  if (curl_errno($ch)) {
      return 'Error:' . curl_error($ch);
  }
  $json = json_decode($result, true);

  //Array debug
  //print_r($json);

  if (count($json, 1) != 0) {
      $temp['name']  = $json["data"][0]["display_name"];
      curl_close($ch);
      return $temp['name'];
    }
  }
}

function getchatters($conn,$client_id_admin){
  $stmt = $conn->prepare("SELECT twitch_id,oauthtoken,showcount FROM users WHERE username = :username");
  $stmt->bindValue(':username', $_SESSION['username']);
  $stmt->execute();
  $array = $stmt->fetch(PDO::FETCH_ASSOC);
  if($array['showcount'] == '1'){
    $twitchname = getname($conn,$client_id_admin);
    $oauthtoken = $array['oauthtoken'];
    if (!empty($twitchname)) {
      $twitchname = strtolower($twitchname);
    //Sanitize URL to make it easier for cURL and easier to read
    $url = "https://tmi.twitch.tv/group/user/" . $twitchname . "/chatters";
    $ch  = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


    $headers   = array();
    $headers[] = "Client-Id: " . $client_id_admin;
    $headers[] = "Authorization: Bearer " . $oauthtoken;
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        return 'Error:' . curl_error($ch);
    }
    $json = json_decode($result, true);

    //Array debug
    //return $json;

    if (count($json, 1) != 0) {
        $temp['chatter']  = $json["chatter_count"];
        curl_close($ch);
        return $temp['chatter'];
      }
    }
  } else {
    $hiddentext = 'Hidden';
    return $hiddentext;
  }
}

function getviewercount($conn,$client_id_admin) {
  $stmt = $conn->prepare("SELECT twitch_id,oauthtoken,showcount FROM users WHERE username = :username");
  $stmt->bindValue(':username', $_SESSION['username']);
  $stmt->execute();
  $array = $stmt->fetch(PDO::FETCH_ASSOC);
  $channel_id = $array['twitch_id'];
  $oauthtoken = $array['oauthtoken'];
  if($array['showcount'] == '1'){
    if(!empty($channel_id) && !empty($oauthtoken)){
      //Sanitize URL to make it easier for cURL and easier to read
      $url = "https://api.twitch.tv/helix/streams?user_id=".$channel_id;
      $ch  = curl_init();

      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


      $headers   = array();
      $headers[] = "Client-Id: " . $client_id_admin;
      $headers[] = "Authorization: Bearer " . $oauthtoken;
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);
      if (curl_errno($ch)) {
          echo 'Error:' . curl_error($ch);
      }
      $json = json_decode($result, true);

      //Array debug
      //print_r($json);

      if(count($json["data"], 1) != 0) {
          if(!empty($json["data"][0]["type"])){
             return $json["data"][0]["viewer_count"];
          } else {
             return false;
          }
      }
         curl_close($ch);
      }
  } else {
    return false;
  }
}
 ?>
