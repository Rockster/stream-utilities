$(window).ready(function() {
  $('#overlay').fadeTo(2500, 1);
  $('#dvLoading').fadeOut(2500);
});
$(window).on('load', (function() {
  $('#todolist').load('template/todo.html')
}));
if ($("#statviewer").length) {
  $(window).on('load', (function() {
    $('#statviewer').load('template/statviewerlook.php')
  }));
}
$(document).ready(function() {
  $('#todo').click(function() {
    var link = $(this);
    $('#todolist').slideToggle(1000, function() {
      if ($(this).is(":visible")) {
        link.text('Click here to hide the to-do list');
      } else {
        link.text('Click here to check the To-do list');
      }
    })
  })
});
$(document).ready(function() {
  $('#stats').click(function() {
    var link = $(this);
    $('#statviewer').slideToggle(1000, function() {
      if ($(this).is(":visible")) {
        link.text('Click here to hide your stats!');
      } else {
        link.text('Click here to check your stats!');
      }
    })
  })
});
$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-off");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

$(window).on('load', (function() {
  $('#gdpr').load('template/modal/privacy.html')
}));
$(window).on('load', (function() {
  $('#about').load('template/modal/about.html')
}));
$(document).ready(function() {
  var close = document.getElementsByClassName("closebtn");
  var i;
  for (i = 0; i < close.length; i++) {
    $('.closebtn').click(function() {
      var div = this.parentElement;
      div.style.opacity = "0";
      setTimeout(function() {
        div.style.display = "none";
      }, 600);
    })
  }
  if (Cookies.get('cookie')) {
    $(window).on('load', (function() {
      $('#ads').load('template/ads.html')
    }));
    var adblockDetect = window.adblockDetect;
    if (adblockDetect) {
      $("#ads").html("<div id='adblockon'>Please consider turning off your adblocking software. This helps out the website a bunch and it allows us to stay free!</div>");
    } else {
      console.log('Adblock is disabled just run the ads');
    };
  }

  var closeupdate = document.getElementsByClassName("closebtnupdate");
  var x;
  for (x = 0; x < closeupdate.length; x++) {
    $('.closebtnupdate').click(function() {
      var div = this.parentElement;
      div.style.opacity = "0";
      Cookies.set('update', 'true', {
        expires: 14
      }, {
        samesite: 'none',
        secure: true
      });
      setTimeout(function() {
        div.style.display = "none";
      }, 600);
    })
  }
  if (Cookies.get('theme') == 'dark') {
    $('.switch').toggle();
    Cookies.remove('theme');
    Cookies.set('theme', 'dark', {
      expires: 365
    }, {
      samesite: 'none',
      secure: true
    });
  }
  $(document).ready(function() {
    if (!Cookies.get('theme')) {
      if(Cookies.get('cookie')) {
      const userPrefersDark = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
      const userPrefersLight = window.matchMedia && window.matchMedia('(prefers-color-scheme: light)').matches;
      if (userPrefersDark) {
        $('.switch').toggle();
        $('link[href="web-img/css/style.css"]').attr('href', 'web-img/css/dark.css');
        Cookies.set('theme', 'dark', {
          expires: 365
        }, {
          samesite: 'none',
          secure: true
        });
      } else {
        $('link[href="web-img/css/dark.css"]').attr('href', 'web-img/css/style.css');
      }
    }}
  });
  $('.switch').click(function() {
    if (Cookies.get('theme') == 'dark') {
      $('link[href="web-img/css/dark.css"]').attr('href', 'web-img/css/style.css');
      Cookies.remove('theme');
      $('.switch').toggle();
    } else {
      $('link[href="web-img/css/style.css"]').attr('href', 'web-img/css/dark.css');
      Cookies.set('theme', 'dark', {
        expires: 365
      }, {
        samesite: 'none',
        secure: true
      });
      $('.switch').toggle();
    };
  });
});
$("document").ready(function() {
  var modal1 = document.getElementById("gdpr");
  $('.gdprBtn').click(function() {
    $('#gdpr').slideDown();
  })
  $('.closegdpr').click(function() {
    $('#gdpr').slideUp();
  })
  $(window).click(function(e) {
    if (event.target == modal1) {
      $('#gdpr').slideUp();
    }
  })
});
$("document").ready(function() {
  var modal2 = document.getElementById("about");
  $('.aboutBtn').click(function() {
    $('#about').slideDown();
  })
  $('.closeabout').click(function() {
    $('#about').slideUp();
  })
  $(window).click(function(e) {
    if (event.target == modal2) {
      $('#about').slideUp();
    }
  })
});
$(document).ready(function() {
  $('.agree').click(function() {
    Cookies.set('cookie', 'true', {
      expires: 365
    }, {
      sameSite: 'strict'
    }, {
      secure: true
    });
    location.reload();
  })
});
