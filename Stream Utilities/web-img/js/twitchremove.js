$(window).on('load', (function() {
  var modaltwitch = document.getElementById("removetwitch");
  var btntwitch = document.getElementById("removetwitchbtn");
  var spantwitch = document.getElementsByClassName("close")[0];
  btntwitch.onclick = function() {
    $('#removetwitch').slideDown();
  }
  spantwitch.onclick = function() {
    $('#removetwitch').slideUp();
  }
  document.onclick = function(event) {
    if (event.target == modaltwitch) {
      $('#removetwitch').slideUp();
    }
  }
  $('#removeno').on('click', function() {
    $('#removetwitch').slideUp();
  });
}));
