$(window).on('load', (function() {
  $('#myModal').load('template/modal/bannerhelp.html')
}));
$("document").ready(function() {
  var modalbanner = document.getElementById("myModal");
  var btnbanner = document.getElementById("myBtn");
  var spanbanner = document.getElementsByClassName("close")[0];
  btnbanner.onclick = function() {
    $('#myModal').slideDown();
  }
  spanbanner.onclick = function() {
    $('#myModal').slideUp();
  }
  document.onclick = function(event) {
    if (event.target == modalbanner) {
      $('#myModal').slideUp();
    }
  }
});

(function() {
  const form = document.querySelector('#form');
  const checkboxes = form.querySelectorAll('input[type=checkbox]');
  const checkboxLength = checkboxes.length;
  const firstCheckbox = checkboxLength > 0 ? checkboxes[0] : null;

  function init() {
    if (firstCheckbox) {
      for (let i = 0; i < checkboxLength; i++) {
        checkboxes[i].addEventListener('change', checkValidity);
      }

      checkValidity();
    }
  }

  function isChecked() {
    for (let i = 0; i < checkboxLength; i++) {
      if (checkboxes[i].checked) return true;
    }

    return false;
  }

  function checkValidity() {
    const errorMessage = !isChecked() ? 'At least one checkbox must be selected.' : '';
    firstCheckbox.setCustomValidity(errorMessage);
  }

  init();
})();

$(function() {
  //show it when the checkbox is clicked
  $('input[name="twitterbool"]').on('click', function() {
    if ($(this).prop('checked')) {
      $('#twitter').fadeIn();
      $("#twittername").prop('disabled', false);
      $("#twittername").prop('required', true);
    } else {
      $('#twitter').hide();
      $("#twittername").prop('disabled', true);
      $("#twittername").prop('required', false);
    }
  });
});
$(function() {
  //show it when the checkbox is clicked
  $('input[name="discordbool"]').on('click', function() {
    if ($(this).prop('checked')) {
      $('#discord').fadeIn();
      $("#discordname").prop('disabled', false);
      $("#discordname").prop('required', true);
    } else {
      $('#discord').hide();
      $("#discordname").prop('disabled', true);
      $("#discordname").prop('required', false);
    }
  });
});
$(function() {
  //show it when the checkbox is clicked
  $('input[name="instagrambool"]').on('click', function() {
    if ($(this).prop('checked')) {
      $('#instagram').fadeIn();
      $("#instagramname").prop('disabled', false);
      $("#instagramname").prop('required', true);
    } else {
      $('#instagram').hide();
      $("#instagramname").prop('disabled', true);
      $("#instagramname").prop('required', false);
    }
  });
});
$(function() {
  //show it when the checkbox is clicked
  $('input[name="facebookbool"]').on('click', function() {
    if ($(this).prop('checked')) {
      $('#facebook').fadeIn();
      $("#facebookname").prop('disabled', false);
      $("#facebookname").prop('required', true);
    } else {
      $('#facebook').hide();
      $("#facebookname").prop('disabled', true);
      $("#facebookname").prop('required', false);
    }
  });
});
$(function() {
  //show it when the checkbox is clicked
  $('input[name="snapchatbool"]').on('click', function() {
    if ($(this).prop('checked')) {
      $('#snapchat').fadeIn();
      $("#snapchatname").prop('disabled', false);
      $("#snapchatname").prop('required', true);
    } else {
      $('#snapchat').hide();
      $("#snapchatname").prop('disabled', true);
      $("#snapchatname").prop('required', false);
    }
  });
});
