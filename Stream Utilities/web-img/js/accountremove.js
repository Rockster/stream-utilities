$(window).on('load', (function() {
  var modalaccount = document.getElementById("removeaccount");
  var btnaccount = document.getElementById("removeaccountbtn");
  var spanaccount = document.getElementsByClassName("close")[0];
  btnaccount.onclick = function() {
    $('#removeaccount').slideDown();
  }
  spanaccount.onclick = function() {
    $('#removeaccount').slideUp();
  }
  document.onclick = function(event) {
    if (event.target == modalaccount) {
      $('#removeaccount').slideUp();
    }
  }
  $('#removeno').on('click', function() {
    $('#removeaccount').slideUp();
  });
}));
