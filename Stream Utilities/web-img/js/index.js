$( document ).ready(function() {
  var params = new URLSearchParams(window.location.search);
  var code = params.get('code');
  if(code){
      $("#login-wrapper").fadeOut(0);
      $("#password-wrapper").fadeOut(0);
      $("#register-wrapper").fadeIn(1500);
  }
});

$( document ).ready(function() {
  $("#registerBtn").click(function(){
    $("#login-wrapper").fadeOut(0);
    $("#password-wrapper").fadeOut(0);
    $("#register-wrapper").fadeIn(1500);
  });
  $("#loginbutton").click(function(){
    $("#register-wrapper").fadeOut(0);
    $("#login-wrapper").fadeIn(1500);
  });
  $("#loginbuttonback").click(function(){
    $("#password-wrapper").fadeOut(0);
    $("#login-wrapper").fadeIn(1500);
  });
  $("#password").click(function(){
    $("#register-wrapper").fadeOut(0);
    $("#login-wrapper").fadeOut(0);
    $("#password-wrapper").fadeIn(1500);
  });
});

//Grecaptcha stuff
    grecaptcha.ready(function () {
        grecaptcha.execute('6LcfObcUAAAAABVww4LbZOXXB83forSKFevJ1W59', { action: 'Login' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse');
            recaptchaResponse.value = token;
        });
        grecaptcha.execute('6LcfObcUAAAAABVww4LbZOXXB83forSKFevJ1W59', { action: 'Register' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse-1');
            recaptchaResponse.value = token;
        });
        grecaptcha.execute('6LcfObcUAAAAABVww4LbZOXXB83forSKFevJ1W59', { action: 'Reset' }).then(function (token) {
            var recaptchaResponse = document.getElementById('recaptchaResponse-2');
            recaptchaResponse.value = token;
        });
    });
//End of Grecaptcha


var strength = {
0: "Come on even a kid could guess this...",
1: "Getting better but can be loads stronger.",
2: "I know you can make up a better password.",
3: "Nearly there just some more characters.",
4: "Holy smokes this is a strong password :O"
}
var password = document.getElementById('InputPassword');
var text = document.getElementById('password-strength-text');
if(password){
password.addEventListener('input', function() {
  var val = password.value;
  var result = zxcvbn(val);

  // Update the text indicator
  if (val !== "") {
    $('#password-strength-text').fadeIn('fast');
    text.innerHTML = "Password strength: <br>" + strength[result.score] + "<br><br>Your password must atleast have one number, one uppercase letter and have atleast 8 characters in it! ";
  } else {
    text.innerHTML = "";
  }
})};
