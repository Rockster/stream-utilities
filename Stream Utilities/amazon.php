<?php
include "../smart/config.ini.php";
include "functions/content.php";
checkvalid($conn);
if(isset($_POST['create'])){
  if(!empty($_POST['gbr']) || !empty($_POST['deu']) || !empty($_POST['usa'])){
    $usa = $_POST['usa'];
    $gbr = $_POST['gbr'];
    $deu = $_POST['deu'];
    $can = $_POST['can'];
    $esp = $_POST['esp'];
    $jpn = $_POST['jpn'];
    $fra = $_POST['fra'];
    if(createamazon($usa, $gbr, $deu, $can, $esp, $jpn, $fra, $conn)){
      echo "<div class='alert success'><span class='closebtn'>&times;</span><strong>Created!</strong><br> You created your true one link!<br>Your unique link is https://rockster.dev/amazon/".$_SESSION["username"]."</div>";
    } else {
      echo "<div class='alert'><span class='closebtn'>&times;</span><strong>Error!</strong><br> Creation failed try again later!</div>";
    }
  } else {
    echo "<div class='alert'><span class='closebtn'>&times;</span><strong>Error!</strong><br> You didn't fill in everything that's needed!</div>";
  }
} elseif(isset($_POST['update'])) {
  $gbr = $_POST['gbr'];
  $deu = $_POST['deu'];
  $can = $_POST['can'];
  $esp = $_POST['esp'];
  $jpn = $_POST['jpn'];
  $fra = $_POST['fra'];
  if(updateamazon($gbr, $deu, $can, $esp, $jpn, $fra,$conn)){
    echo "<div class='alert success'><span class='closebtn'>&times;</span><strong>Updated!</strong><br> Your true one link got updated!</div>";
  } else {
    echo "<div class='alert'><span class='closebtn'>&times;</span><strong>Error!</strong><br> Your true one link didn't get updated!</div>";
  }
}
?>
<html>
<head>
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
  <link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script async src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
    <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
    <script src="web-img/js/amazon.js"></script>
    <script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script>
    <style>
    .alert {
      padding: 15px;
      background-color: #ee1111;
      color: white;
      opacity: 1;
      transition: opacity 0.6s;
      margin-bottom: 15px;
      width: auto;
      text-align: center;
      border-radius: .25rem;
    }
    </style>
</head>
	<body>
    <div class='row'>
    <form action='' method='post' style='margin-block-end: 0;'>
      <div class='column small'><button type='submit' name='submit' class='btn btn-default btn-sm' />
      <span class='fa fa-sign-out-alt'></span> Log out</button></div>
    </form>
			<div class='column small'><button onclick='location.href="dash"' class='btn btn-default btn-sm'><span class='fa fa-home'></span> Go back to Dash</button></div>
    </div>

<div id='formpadding'>
  <div class='row'>

<?php
  $array = checkamazonlink($conn);
  include($array['text']);
 ?>

  </div>
</div>
    <?php
    include 'footer.php';
     ?>

	</body>
</html>
