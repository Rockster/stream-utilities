<?php
require_once 'functions/class.style.php';
if(!isset($_COOKIE['cookie'])){
  echo "<footer style='margin-bottom: 100px;'>";
} else {
  echo "<footer>";
}
echo $style->getstyle();
 ?>
<div id="ads">
</div>
<br>
<p>This site is based on <a href="http://getbootstrap.com/" target='_blank' rel='noreferrer'>Bootstrap CSS</a> | Created by <a href="https://twitch.tv/rockster_dev" target='_blank' rel='noreferrer'>Rockster</a> | Contact : <a
    href="mailto:contact@rockster.dev">Contact@rockster.dev</a><br>
  <span class="gdprBtn">Privacy Policy</span> | <span class="aboutBtn">About us</span> | <a href="https://status.rockster.dev" target='_blank' rel='noreferrer'>Status</a></p>
<p>All rights reserved by Rockster</p>
<p>Stream utilities is still under development if you see any errors contact Rockster</p>
<?php
if(!isset($_COOKIE['cookie'])){
  echo "You first have to accept the cookie banner before you can switch Theme's";
} else {
  echo "<span class='switch'>Click to change to Dark theme</span>
  <span class='switch' style='display:none'>Click to change to Light theme</span>";
}
 ?>

</footer>
<div id="gdpr" class="modal">
</div>
<div id="about" class="modal">
</div>
<?php
if(!isset($_COOKIE['cookie'])){
  if(!isset($_GET['cookie'])){
  echo '<div id="cookie">';
  include('template/cookie.html');
  echo '</div>';
}
} else {
// do nothing
}
?>
<!DOCTYPE html>
<title><?php echo $pageTitle;?></title>
<script async src="https://cdnjs.cloudflare.com/ajax/libs/adblock-detect/1.0.5/index.min.js" rel="preload" integrity="sha256-DlycQwxDAnNVHEbmnVi+wHbEFxpB9W7wQR5nCnZlGnw=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="web-img/css/fontmaker.css"/>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.4.2/zxcvbn.js" rel="preload" integrity="sha256-Znf8FdJF85f1LV0JmPOob5qudSrns8pLPZ6qkd/+F0o=" crossorigin="anonymous"></script>
<script defer src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js" rel="preload" integrity="sha256-oE03O+I6Pzff4fiMqwEGHbdfcW7a3GRRxlL+U49L5sA=" crossorigin="anonymous"></script>
<script src="web-img/js/include.js"></script>
