<?php
include "../smart/config.ini.php";
include "functions/content.php";
if(!$security->checkadmin($conn)){
  header('location:dash');
}

$code = createbot($conn);
$uniqueid = $_SESSION['UniqueId'];
if(isset($_POST[$uniqueid])){
    if($code['bool'] === false && empty($code['redirect']) && !empty($code['message'])){
      echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error!</strong><br>' . $code['message'] . '</div>';
    } elseif($code['bool'] == true && !empty($code['message'])) {
        echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>' . $code['message'] . '</div>';
    } elseif(!empty($code['redirect'])){
      header('location:'.$code['redirect']);
    }
}

?>
<!DOCTYPE html>
<html>

<head>
  <link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
  <link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
  <meta name="application-name" content="&nbsp;" />
  <meta name="msapplication-TileColor" content="#FFFFFF" />
  <meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
  <title><?php echo $pageTitle;?></title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script async src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
  <script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
  </script>
  <style>
    .alert {
      padding: 15px;
      background-color: #ee1111;
      color: white;
      opacity: 1;
      transition: opacity 0.6s;
      margin-bottom: 15px;
      width: 25%;
      margin: auto;
      text-align: center;
      border-radius: .25rem;
    }

    .tooltip {
      position: relative;
      display: inline-block;
    }

    .tooltip .tooltiptext {
      visibility: hidden;
      background-color: #555;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px;
      position: absolute;
      z-index: 1;
      bottom: 85%;
      left: 50%;
      margin-left: -75px;
      opacity: 0;
      transition: opacity 0.3s;
    }

    .tooltip .tooltiptext::after {
      content: "";
      position: absolute;
      top: 100%;
      left: 70px;
      margin-left: -5px;
      border-width: 5px;
      border-style: solid;
      border-color: #555 transparent transparent transparent;
    }

    .tooltip:hover .tooltiptext {
      visibility: visible;
      opacity: 1;
    }

    #table {
      padding-top: 10px;
    }

    table {
      border-collapse: collapse;
      border-spacing: 0;
      width: 100%;
    }

    th,
    td {
      text-align: left;
      padding: 16px;
    }

    tr:nth-child(even) {
      background-color: #43495282;
    }

    .column {
      float: left;
      padding: 10px;
    }

    .column.small {
      float: left;
      padding: 10px;
    }

    .row:after {
      content: "";
      display: table;
      clear: both;
    }

    @media screen and (max-width: 600px) {
      .column {
        width: 100%;
      }
    }
  </style>
</head>
<body>
  <?php
   if(isset($_COOKIE['theme'])){
   		echo '<div id="dvLoading"><img class="imageloader" src="../../web-img/images/loader_white.webp"></div><div id="overlay">';
   } else {
    	echo '<div id="dvLoading"><img class="imageloader" src="../../web-img/images/loader_black.webp"></div><div id="overlay">';
	 }
             ?>
             <div class="row">
             <form action="" method="post" style="margin-block-end: 0;">
               <div class='column small'><button type="submit" name='submit' class="btn btn-default btn-sm" />
               <span class="fa fa-sign-out-alt"></span> Log out</button></div>
             </form>
         			<div class='column small'><button onclick="location.href='dash'" class="btn btn-default btn-sm"><span class="fa fa-home"></span> Go back to Dash</button></div>
             </div>

    <?php

    if(checktwitch($conn)){
      echo "<div class='row'><div class='column small'>Before being able to use Code generation please link your Twitch by clicking on this button<br>".checktwitch($conn) . "</div></div>";
    } else {
      $getbot = getbot($conn);
      //print_r($getcode); //Is for debugging
      if(!empty($getbot)){
        echo "<div id='formpadding'><div id='table'><table><tr><th>Oauth</th></tr>";
        foreach($getbot as $msg) {
        echo $msg['text'];
      }
        echo "</table></div>";
      } else {
   echo '<br>
   <div class="row">
   <div class="column small">
   <form method="post">
      <input type="text" class="form-control" value="'.$code['code'].'" id="myInput" style="width: 800px;display: inline;" readonly>
        <button type="submit" name="'.$_SESSION['UniqueId'].'" class="btn btn-primary" value="'.$code['code'].'">
      <span class="fa fa-plus-square"></span> Create bot linking code</button>
   </form>
   </div>';
 }
}
     ?>

  </div>
<script>
  function myFunction() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");

    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copied: " + copyText.value;
  }

  function outFunc() {
    var tooltip = document.getElementById("myTooltip");
    tooltip.innerHTML = "Copy to clipboard";
  }
</script>
<?php
   include "footer.php";
   ?>
</div>
</body>
</html>
