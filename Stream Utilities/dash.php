<?php
	require_once "../smart/config.ini.php";
	require_once "functions/content.php";
	require_once 'functions/class.style.php';
	$refreshtoken = refreshtoken($conn, $client_id_admin, $client_secret_admin);
	checkvalid($conn);
	?>
<html>
	<head>
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
		<link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
		<meta name="application-name" content="&nbsp;" />
		<meta name="msapplication-TileColor" content="#FFFFFF" />
		<meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
		<script async src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
		<script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
		<script src="web-img/js/dash.js"></script>
		<script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js"></script>
		<style>
			.alert {
			padding: 15px;
			background-color: #ee1111;
			color: white;
			opacity: 1;
			transition: opacity 0.6s;
			margin-bottom: 15px;
			width: auto;
			text-align: center;
			border-radius: .25rem;
			}
		</style>
	</head>
	<body>
		<?php
			$amazoncheck = checkamazon($conn);
			echo $style->getspinner();
      if(isset($refreshtoken['exception'])){
        echo "<div class='alert warning'><span class='closebtn'>&times;</span><strong>Error!</strong>".$refreshtoken['exception']."</div>";
      }
			if(isset($amazoncheck['exception'])){
			  echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error! </strong><br>'.$amazoncheck['exception'].'</div>';
			}
			if(isset($_GET['success']) && $_GET['success'] == 'true') {
			  echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success! </strong><br>Linking twitch has been succeeded you will see some new features popup soon!</div>';
			} elseif(isset($_GET['success']) && $_GET['success'] == 'false')  {
			  echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error! </strong><br>Linking twitch has failed try again later!</div>';
			}
			if(isset($_GET['notallowed'])){
			  echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error! </strong><br>You are not allowed to do that!</div>';
			}
			if(!isset($_COOKIE['update'])){
			  echo '<div class="alert info"><span class="closebtnupdate">&times;</span><strong>Info!</strong><br>Be sure to have your OBS/Xsplit/Slobs updated for the newest addition in stream utilities!</div>';
			}
			  ?>
				<div class='row'>
				<form action='' method='post' style='margin-block-end: 0;'>
					<div class='column small'><button type='submit' name='submit' class='btn btn-default btn-sm' />
					<span class='fa fa-sign-out-alt'></span> Log out</button></div>
				</form>
		<div class='column small'><button onclick='location.href="settings"' class='btn btn-default btn-sm'><span class='fa fa-cog'></span> Settings</button></div>
		<?php
		if(checkadmin($conn)){
			echo '<div class="column small"><button onclick="location.href=\'createbot\'" class="btn btn-default btn-sm"><span class="fa fa-cog"></span> Create bot json</button></div>';
		}
		?>
	</div>
		<div class="row">
			<?php
				if(isset($amazoncheck['button'])){
				  echo $amazoncheck['button'];
				}
				?>
			<div class='column small'><button onclick="location.href='banner'" class='btn btn-default btn-sm'><span class='fa fa-plus-square'></span> Go to banner creation</button></div>
			<?php
				if(checktwitch($conn)){
					echo "<div class='column small'>".checktwitch($conn) . "</div>";
				}
				if(!checktwitch($conn)){
				  echo "<div class='column small'><button onclick='location.href=\"codegenerate\"' class='btn btn-default btn-sm'><span class='fa fa-plus-square'></span> Go to code generation</button></div>";
				}
				?>
		</div>
		<br>
		<div>
			<div class='row'>
				<div class='column'>
					<div id='todo'>Click here to check the To-do list</div>
					<span id='todolist' style='display: none;'>
					</span>
				</div>
			<?php
			if(!checktwitch($conn)){
				echo "<div class='column'>
				<div id='stats'>Click here to check your stats!</div>
				<span id='statviewer'style='display: none;'>
				<br>
			</div>";
		}
			?>
		</div>
	</body>
	<script>

	</script>
	<?php
		include "footer.php";
		?>
	</div>
</html>
