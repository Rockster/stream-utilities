<?php
include_once('errorhandling.php');
header_remove('x-powered-by');
$currentCookieParams = session_get_cookie_params();
//print_r($currentCookieParams);
$rootDomain = '.rockster.dev';
session_set_cookie_params($currentCookieParams["lifetime"],$currentCookieParams["path"],$rootDomain,true,true);
$pagename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
if($pagename == "index" || $pagename == "admin" || $pagename == "stream" || strpos($pagename, '?code=') !== false){
  $pagename = "Login";
  $pageTitle = 'Stream Utilities: '.$pagename;
} elseif($pagename == "reset") {
  $pagename = "Reset password";
  $pageTitle = 'Stream Utilities: '.$pagename;
} else {
  $pagename = ucfirst($pagename);
  $pageTitle = 'Stream Utilities: '.$pagename;
}
// Recaptcha secret key for global use in content.php
$recaptcha_secret = '';

//Client ID and secret needed for statviewer and for global use in content.php i.e Refreshtoken and linking twitch
$client_id_admin = '';
$client_secret_admin = '';

//Database configuration PDO
$servername = "localhost";
$username   = "";
$password   = "";


try {
    $conn = new PDO("mysql:host=$servername;dbname=", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    error_log($e);
    echo "An exception has occured!";
}
catch (Exception $e) {
    echo "An exception has occured!";
}




//Check if site is in maintenance mode
$stmt = $conn->prepare("SELECT maintain FROM settings");
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
$maintenance = $result['maintain'];


if (isset($_GET['redirected'])) {
    //do nothing
} else {
    if ($maintenance === '1') {
        header("Location: /maintenance?redirected=true");
        exit;
    } else {
        // do nothing
    }
}
if (!preg_match('(api)', $_SERVER['REQUEST_URI'])) {
  $disableerrors = true;
    if (!preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"])) {
        echo '<script src="../../../../../../web-img/js/console.js"></script>';
    }
} else {
    //do nothing since it breaks the bot
}
if(session_status() == PHP_SESSION_NONE){
    //session has not started
    session_start();
}
