<?php

/**
* Error handler, passes flow over the exception logger with new ErrorException.
*/
function log_error( $num, $str, $file, $line )
{
    log_exception( new ErrorException( $str, 0, $num, $file, $line ) );
}


/**
* Uncaught exception handler.
*/
function log_exception( Throwable $e )
{
        $date = date_create();
        $pagename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
        echo "<link rel='apple-touch-icon-precomposed' sizes='57x57' href='/web-img/favicon/apple-touch-icon-57x57.webp' />"
            ."<link rel='icon' type='image/webp' href='/web-img/favicon/favicon-32x32.webp' sizes='32x32' />"
            ."<meta name='application-name' content='&nbsp;'/>"
            ."<meta name='msapplication-TileColor' content='#FFFFFF' />"
            ."<meta name='msapplication-TileImage' content='/web-img/favicon/mstile-144x144.webp' />"
            ."<link rel='stylesheet' type='text/css' href='/web-img/css/error.css'>"
            ."</head>"
            ."<video autoplay muted loop id='myVideo' style='z-index:9001;'>"
            ."<source src='/web-img/video/errorpage.mp4' type='video/mp4'>"
            ."</video><div style='display: grid;place-items: center;z-index:9002;' class='errorcontainer'>"
            ."<h1>Something went totally wrong</h1>"//Will add randomized titles at some point lmao
            ."<table style='color:black!important;'>"
            ."<tr><th style='width: 130px!important;'>What happened?</th><td> A " . get_class( $e ) ." occured</td></tr>"
            ."<tr><th>Message</th><td> {$e->getMessage()}</td></tr>"
            ."<tr><th>File</th><td> ".$pagename."</td></tr>"
            ."<tr><th>Line</th><td> {$e->getLine()}</td></tr>"
            ."</table>"
            ."<footer>This site is based on <a href='http://getbootstrap.com/' target='_blank'>Bootstrap CSS</a> | Created by <a href='https://twitch.tv/rockster_dev' target='_blank'>Rockster</a> | Contact : <a href='mailto:contact@rockster.dev'>Contact@rockster.dev</a></footer></div>";
        $message = "Date: ".date_format($date, 'y-m-j H:i:s:u')." Type: " . get_class( $e ) . "; Message: {$e->getMessage()}; File: {$e->getFile()}; Line: {$e->getLine()};\n";
        file_put_contents("../pages/exceptions.ini.txt", $message, FILE_APPEND );
        //header( "Location: {$config["error_page"]}" );

    exit();
}

/**
* Checks for a fatal error, work around for set_error_handler not working on fatal errors.
*/
function check_for_fatal()
{
    $error = error_get_last();
    $date = date_create();
    $pagename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
    if(!empty($error)){
    if ( $error["type"] == E_ERROR || $error["type"] == E_PARSE ){
      log_error( $error["type"], $error["message"], $error["file"], $error["line"] );

    echo "<link rel='apple-touch-icon-precomposed' sizes='57x57' href='/web-img/favicon/apple-touch-icon-57x57.webp' />"
        ."<link rel='icon' type='image/webp' href='/web-img/favicon/favicon-32x32.webp' sizes='32x32' />"
        ."<meta name='application-name' content='&nbsp;'/>"
        ."<meta name='msapplication-TileColor' content='#FFFFFF' />"
        ."<meta name='msapplication-TileImage' content='/web-img/favicon/mstile-144x144.webp' />"
        ."<link rel='stylesheet' type='text/css' href='/web-img/css/error.css'>"
        ."</head>"
        ."<div>"
        ."<video autoplay muted loop id='myVideo' style='z-index:9001;'>"
        ."<source src='/web-img/video/errorpage.mp4' type='video/mp4'>"
        ."</video><div style='display: grid;place-items: center;z-index:9002;' class='errorcontainer'>"
        ."<h1>Some idiot didn't do his job properly</h1>"//Will add randomized titles at some point lmao
        ."<table style='color:black!important;'>"
        ."<tr><th style='width: 130px;'>What happened?</th><td> A " .  $error["type"] . " occured</td></tr>"
        ."<tr><th>Message</th><td>  " .  $error["message"] . " </td></tr>"
        ."<tr><th>Which file?</th><td> ".$pagename."</td></tr>"
        ."<tr><th>On line</th><td> line " .  $error["line"] . " </td></tr>"
        ."</table>"
        ."<footer>This site is based on <a href='http://getbootstrap.com/' target='_blank'>Bootstrap CSS</a> | Created by <a href='https://twitch.tv/rockster_dev' target='_blank'>Rockster</a> | Contact : <a href='mailto:contact@rockster.dev'>Contact@rockster.dev</a></footer></div></div>";
        $message = "Date: ".date_format($date, 'y-m-j H:i:s:u')." Type: " . $error["type"] . "; Message: ".$error["message"]." File: ".$error["file"]." Line: ".$error["line"]."\n";
        file_put_contents("../pages/fatalerror.ini.txt", $message, FILE_APPEND );
        exit();
    }
  }
}

register_shutdown_function( "check_for_fatal" );
set_error_handler( "log_error" );
set_exception_handler( "log_exception" );
ini_set( "display_errors", "off" );
error_reporting( E_ALL );
