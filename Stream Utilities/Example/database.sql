-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 29, 2019 at 02:34 PM
-- Server version: 10.3.15-MariaDB-cll-lve
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Database`
--

-- --------------------------------------------------------

--
-- Table structure for table `amazon`
--

CREATE TABLE `amazon` (
  `affiliate` varchar(64) NOT NULL,
  `usa` varchar(8) NOT NULL,
  `gbr` varchar(8) NOT NULL,
  `deu` varchar(8) NOT NULL,
  `can` varchar(8) DEFAULT NULL,
  `esp` varchar(8) DEFAULT NULL,
  `jpn` varchar(8) DEFAULT NULL,
  `fra` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `affiliate` varchar(64) NOT NULL,
  `code` varchar(128) NOT NULL,
  `twitter` enum('1','0') NOT NULL,
  `discord` enum('1','0') NOT NULL,
  `instagram` enum('1','0') NOT NULL DEFAULT '0',
  `facebook` enum('1','0') NOT NULL DEFAULT '0',
  `snapchat` enum('1','0') NOT NULL DEFAULT '0',
  `discordtext` varchar(64) NOT NULL,
  `twittertext` varchar(64) NOT NULL,
  `instagramtext` varchar(64) NOT NULL,
  `facebooktext` varchar(64) NOT NULL,
  `snapchattext` varchar(64) NOT NULL,
  `effect` enum('fade','slide','direct') NOT NULL DEFAULT 'fade',
  `effecttimer` int(64) NOT NULL DEFAULT 1000,
  `refreshtimer` int(64) NOT NULL DEFAULT 600000,
  `delay` int(64) NOT NULL DEFAULT 2000
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(1) NOT NULL DEFAULT 1,
  `maintain` enum('1','0') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT 0,
  `rank` enum('admin','user') NOT NULL DEFAULT 'user',
  `token` varchar(128) NOT NULL,
  `twitchlink` tinyint(1) NOT NULL DEFAULT 0,
  `oauthtoken` varchar(256) NOT NULL,
  `refreshtoken` varchar(256) NOT NULL,
  `twitch_id` varchar(128) DEFAULT NULL,
  `showcount` enum('1','0') NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `wildcards`
--

CREATE TABLE `wildcards` (
  `verifynumber` varchar(128) NOT NULL,
  `type` enum('EA','IN') NOT NULL DEFAULT 'IN', --EA = Early access IN = Invited with priority
  `byuser` varchar(128) NOT NULL,
  `used` enum('0','1') NOT NULL DEFAULT '0',
  `usedby` varchar(128) NOT NULL,
  `created` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amazon`
--
ALTER TABLE `amazon`
  ADD PRIMARY KEY (`affiliate`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`affiliate`,`code`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `wildcards`
--
ALTER TABLE `wildcards`
  ADD PRIMARY KEY (`verifynumber`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; --This will be changed to a truely random hash based in codegenerator(); within class.security.php
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
