<?php
	require_once "../smart/config.ini.php";
	require_once "functions/class.settings.php";
	$security->checkvalid($conn);
	if(isset($_POST['removetwitchyes'])){
		$array = $settings->removetwitchlink($conn);
		$bool = $array['bool'];
		if ($bool == "invalid" && $bool != true) {
				echo '<div class="alert warning"><span class="closebtn">&times;</span><strong>Warning!</strong><br>' . $array['message'] . '</div>';
		} elseif($bool == true) {
				echo '<div class="alert success"><span class="closebtn">&times;</span><strong>Success!</strong><br>' . $array['message'] . '</div>';
		} elseif($bool === false) {
				echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error!</strong><br>' . $array['message'] . '</div>';
		}
	}
	if(isset($_POST['removeaccountyes'])){
		$settings->removeaccount($conn);
	}
	?>
<html>

<head>
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
	<link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
	<meta name="application-name" content="&nbsp;" />
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script async src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha256-KM512VNnjElC30ehFwehXjx1YCHPiQkOPmqnrWtpccM=" crossorigin="anonymous"></script>
	<script defer src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha256-CjSoeELFOcH0/uxWu6mC/Vlrc1AARqbm/jiiImDGV3s=" crossorigin="anonymous"></script>
	<script async custom-element="amp-auto-ads" src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
	</script>
	<style>
		.alert {
			padding: 15px;
			background-color: #ee1111;
			color: white;
			opacity: 1;
			transition: opacity 0.6s;
			margin-bottom: 15px;
			width: auto;
			text-align: center;
			border-radius: .25rem;
		}

		.column {
			float: left;
			width: 25%;
			padding: 10px;
		}

		.column.small {
			float: left;
			width: auto;
			padding: 10px;
		}

		.row:after {
			content: "";
			display: table;
			clear: both;
		}

		@media screen and (max-width: 600px) {
			.column {
				width: 100%;
			}
		}
	</style>
</head>

<body>
	<?php
	        if(isset($_COOKIE['theme'])){
	  			  echo '<div id="dvLoading"><img class="imageloader" src="../../web-img/images/loader_white.webp"></div><div id="overlay">';
	  			} else {
	  			  echo '<div id="dvLoading"><img class="imageloader" src="../../web-img/images/loader_black.webp"></div><div id="overlay">';
	  			}
					?>
					<div class="row">
			    <form action="" method="post" style="margin-block-end: 0;">
			      <div class='column small'><button type="submit" name='submit' class="btn btn-default btn-sm" />
			      <span class="fa fa-sign-out-alt"></span> Log out</button></div>
			    </form>
						<div class='column small'><button onclick="location.href='dash'" class="btn btn-default btn-sm"><span class="fa fa-home"></span> Go back to Dash</button></div>
			    </div>
					<div class='row'>
	<?php
					if(!$security->checktwitch($conn)){
					  echo "
						<div id='removetwitch' class='modal'>
						</div>
				    <div class='column small'>
						<button class='btn btn-danger' name='removetwitch' id='removetwitchbtn'><span class='fa fa-times'></span> Remove Twitch linking</button>
						</div>";
					}
	  ?>
		<div id='removeaccount' class='modal'>
		</div>
		<div class='column small'>
		<button class='btn btn-danger' name='removeaccount' id='removeaccountbtn'><span class='fa fa-times'></span> Remove account</button>
		</div>
		</div>
</body>
<script>
if($("#removetwitch").length ) {
	$(window).on('load', (function() {$('#removetwitch').load('template/modal/twitchremove.php')}));
}
$(window).on('load', (function() {$('#removeaccount').load('template/modal/accountremove.php')}));
	//$(window).on('load', (function() {$('#settingslayer').load('template/layer/settings.php')})); layer settings work in progress
	$(window).ready(function() {
		$('#overlay').fadeTo(2500, 1);
		$('#dvLoading').fadeOut(2500);
	});
</script>

<?php
		include "footer.php";
		?>
		</div>
</html>
