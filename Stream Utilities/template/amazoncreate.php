<?php checkvalid($conn); ?>
<div class='column small'>
<div id='amazon-last'>
  True one link creator<br>
  <button type='button' class='btn btn-default btn-sm' id='myBtn'>
    <span class='fa fa-info-circle'></span> How does it work?
  </button>
  <div id='myModal' class='modal'>
  </div>
  <h3 style='background-color:red;width: 13%;'>* needs to be filled in!<p>
      Please be sure you are using the page you want to redirect people to!</p>
  </h3>
  <div id='amazon-frame'>
    <form method='POST'>
      <div class='form-group'>
        <label for='usa'>America*</label>
        <input id='usa' name='usa' type='text' aria-describedby='usaHelpBlock' required='required' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <label for='gbr'>United Kingdom</label>
        <input id='gbr' name='gbr' type='text' aria-describedby='gbrHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <label for='deu'>Germany</label>
        <input id='deu' name='deu' type='text' aria-describedby='deuHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <label for='can'>Canada</label>
        <input id='can' name='can' type='text' aria-describedby='canHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <label for='esp'>Spain</label>
        <input id='esp' name='esp' type='text' aria-describedby='espHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <label for='jpn'>Japan</label>
        <input id='jpn' name='jpn' type='text' aria-describedby='jpnHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <label for='fra'>France</label>
        <input id='fra' name='fra' type='text' aria-describedby='fraHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7'>
      </div>
      <div class='form-group'>
        <button type='submit' class='btn btn-primary' name='create'>Create Link</button>
      </div>
    </form>
  </div>
</div>
