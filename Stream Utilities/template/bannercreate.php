<?php checkvalid($conn); ?>
<div id="banner-last">
  Banner creator<br>
  <button type="button" class="btn btn-default btn-sm" id="myBtn">
    <span class="fa fa-info-circle"></span> How does it work?
  </button>
  <div id="myModal" class="modal">
  </div>
  <div id="banner-frame">
    <form id="form" method="post">
      <div class="form-group">

        <p><input type="checkbox" name="twitterbool" value="true">Twitter</p>
        <p><input type="checkbox" name="discordbool" value="true">Discord</p>
        <p><input type="checkbox" name="instagrambool" value="true">Instagram</p>
        <p><input type="checkbox" name="facebookbool" value="true">Facebook</p>
        <p><input type="checkbox" name="snapchatbool" value="true">Snapchat</p>
        <div class="form-group" id="twitter" hidden>
          <label for="twittername">Twitter name</label>
          <input id="twittername" name="twittername" type="text" class="form-control" style="width: 300px;" disabled>
        </div>
        <div class="form-group" id="discord" hidden>
          <label for="discordname">Discord name</label>
          <input id="discordname" name="discordname" type="text" class="form-control" style="width: 300px;" disabled>
        </div>
        <div class="form-group" id="instagram" hidden>
          <label for="instagramname">Instagram name</label>
          <input id="instagramname" name="instagramname" type="text" class="form-control" style="width: 300px;" disabled>
        </div>
        <div class="form-group" id="facebook" hidden>
          <label for="facebookname">Facebook name</label>
          <input id="facebookname" name="facebookname" type="text" class="form-control" style="width: 300px;" disabled>
        </div>
        <div class="form-group" id="snapchat" hidden>
          <label for="snapchatname">Snapchat name</label>
          <input id="snapchatname" name="snapchatname" type="text" class="form-control" style="width: 300px;" disabled>
        </div>
        <div class="form-group">
          <label for="effect">Select effect</label>
          <select class="form-control" id="effect" name="effect" style="width: auto;">
            <option value="fade">Fade in/out</option>
            <option value="slide">Slide down/up</option>
            <option value="direct">Pop in/out</option>
          </select>
        </div>
        <div class="form-group">
          <label for="cycle">Effect cycle (in Milliseconds)</label>
          <input id="cycle" name="cycle" type="number" class="form-control" style="width: 300px;" value="1000" required>
        </div>
        <div class="form-group">
          <label for="delaytimer">Delay timer (in Milliseconds)</label>
          <input id="delaytimer" name="delaytimer" type="number" class="form-control" style="width: 300px;" value="2000" required>
        </div>
        <div class="form-group">
          <label for="refreshtimer">Refresh timer (in Milliseconds)</label>
          <input id="refreshtimer" name="refreshtimer" type="number" class="form-control" style="width: 300px;" value="600000" required>
        </div>
        <p><input type="checkbox" name="rainbowbool" value="true">Rainbow mode?</p>
        <div class="form-group">
          <button type="submit" class="btn btn-primary" name='create' value='<?php echo $security->codegenerator();?>'>Create Banner</button>
        </div>
    </form>
  </div>
  <script>
    (function() {
      const form = document.querySelector('#form');
      const checkboxes = form.querySelectorAll('input[type=checkbox]');
      const checkboxLength = checkboxes.length;
      const firstCheckbox = checkboxLength > 0 ? checkboxes[0] : null;

      function init() {
        if (firstCheckbox) {
          for (let i = 0; i < checkboxLength; i++) {
            checkboxes[i].addEventListener('change', checkValidity);
          }

          checkValidity();
        }
      }

      function isChecked() {
        for (let i = 0; i < checkboxLength; i++) {
          if (checkboxes[i].checked) return true;
        }

        return false;
      }

      function checkValidity() {
        const errorMessage = !isChecked() ? 'At least one checkbox must be selected.' : '';
        firstCheckbox.setCustomValidity(errorMessage);
      }

      init();
    })();

    $(function() {
      //show it when the checkbox is clicked
      $('input[name="twitterbool"]').on('click', function() {
        if ($(this).prop('checked')) {
          $('#twitter').fadeIn();
          $("#twittername").prop('disabled', false);
          $("#twittername").prop('required', true);
        } else {
          $('#twitter').hide();
          $("#twittername").prop('disabled', true);
          $("#twittername").prop('required', false);
        }
      });
    });
    $(function() {
      //show it when the checkbox is clicked
      $('input[name="discordbool"]').on('click', function() {
        if ($(this).prop('checked')) {
          $('#discord').fadeIn();
          $("#discordname").prop('disabled', false);
          $("#discordname").prop('required', true);
        } else {
          $('#discord').hide();
          $("#discordname").prop('disabled', true);
          $("#discordname").prop('required', false);
        }
      });
    });
    $(function() {
      //show it when the checkbox is clicked
      $('input[name="instagrambool"]').on('click', function() {
        if ($(this).prop('checked')) {
          $('#instagram').fadeIn();
          $("#instagramname").prop('disabled', false);
          $("#instagramname").prop('required', true);
        } else {
          $('#instagram').hide();
          $("#instagramname").prop('disabled', true);
          $("#instagramname").prop('required', false);
        }
      });
    });
    $(function() {
      //show it when the checkbox is clicked
      $('input[name="facebookbool"]').on('click', function() {
        if ($(this).prop('checked')) {
          $('#facebook').fadeIn();
          $("#facebookname").prop('disabled', false);
          $("#facebookname").prop('required', true);
        } else {
          $('#facebook').hide();
          $("#facebookname").prop('disabled', true);
          $("#facebookname").prop('required', false);
        }
      });
    });
    $(function() {
      //show it when the checkbox is clicked
      $('input[name="snapchatbool"]').on('click', function() {
        if ($(this).prop('checked')) {
          $('#snapchat').fadeIn();
          $("#snapchatname").prop('disabled', false);
          $("#snapchatname").prop('required', true);
        } else {
          $('#snapchat').hide();
          $("#snapchatname").prop('disabled', true);
          $("#snapchatname").prop('required', false);
        }
      });
    });
  </script>
