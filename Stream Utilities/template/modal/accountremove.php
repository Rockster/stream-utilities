<?php
require_once "../../../smart/config.ini.php";
require_once "../../functions/class.security.php";
$security->checkvalid($conn);
?>
<div class="modal-content">
  <span class="close">&times;</span>
  <div class="modal-header">
    <h2>Are you sure you wanna remove your account?</h2>
  </div>
  <div id='explain-4'>
    <p>By clicking Yes you will remove any trace of your account in the Database including but not limited to One Link, Banners, etc.<br>
      Invite codes to Early access will keep existing under another name then your removed account name!<br>
    <b>Once a account is removed there is no way back!</b></p>
  </div>
  <div class='row'>
    <div class='column small'>
      <button class='btn btn-danger' name='removeaccountno' id='removeno'><span class='fa fa-times'></span> No</button>
    </div>
    <div class='column small'>
      <form method='post'>
        <button class='btn btn-success' type='submit' name='removeaccountyes'><span class='fa fa-check'></span> Yes</button>
      </form>
    </div>
  </div>
</div>
<script>
  $("document").ready(function() {
    var modalaccount = document.getElementById("removeaccount");
    var btnaccount = document.getElementById("removeaccountbtn");
    var spanaccount = document.getElementsByClassName("close")[0];
    btnaccount.onclick = function() {
      $('#removeaccount').slideDown();
    }
    spanaccount.onclick = function() {
      $('#removeaccount').slideUp();
    }
    document.onclick = function(event) {
      if (event.target == modalaccount) {
        $('#removeaccount').slideUp();
      }
    }
    $('#removeno').on('click', function() {
      $('#removeaccount').slideUp();
    });
  });
</script>
