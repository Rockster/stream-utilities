<?php
require_once "../../../smart/config.ini.php";
require_once "../../functions/class.security.php";
$security->checkvalid($conn);
?>
<div class="modal-content">
  <span class="close">&times;</span>
  <div class="modal-header">
    <h2>Are you sure you wanna remove Twitch linking?</h2>
  </div>
  <div id='explain-4'>
    <p>By clicking Yes you will remove any trace of your Twitch linking in the Database. Do not use this feature if you don't have to!<br>You can always re-link your twitch if you accidentally remove it on the dashboard</p>
  </div>
  <div class='row'>
    <div class='column small'>
      <button class='btn btn-danger' name='removetwitchno' id='removeno'><span class='fa fa-times'></span> No</button>
    </div>
    <div class='column small'>
      <form method='post'>
        <button class='btn btn-success' type='submit' name='removetwitchyes'><span class='fa fa-check'></span> Yes</button>
      </form>
    </div>
  </div>
</div>
<script>
  $("document").ready(function() {
    var modaltwitch = document.getElementById("removetwitch");
    var btntwitch = document.getElementById("removetwitchbtn");
    $('#removetwitchbtn').click(function() {
      $('#removetwitch').slideDown();
    })
    $('.close').click(function() {
      $('#removetwitch').slideUp();
    })
    $(window).click(function(e) {
      if (event.target == modaltwitch) {
        $('#removetwitch').slideUp();
      }
    })
    $('#removeno').on('click', function() {
      $('#removetwitch').slideUp();
    });
  });
</script>
