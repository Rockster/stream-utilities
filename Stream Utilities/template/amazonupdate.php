<?php checkvalid($conn); ?>
<div>
<table align='center'>
<tr>
  <th>
    Unique link
  </th>
</tr>
<tr>
  <td>
    <a href='https://rockster.dev/amazon/<?php echo $_SESSION['username'];?>'>https://rockster.dev/amazon/<?php echo $_SESSION['username'];?></a>
  </td>
</tr>
</table>
<hr style='width: 304px;'>
</div>

<div class='column small'>
<div id='amazon-last'>
 True one link updater<br>
  <button type='button' class='btn btn-default btn-sm' id='myBtn'>
    <span class='fa fa-info-circle'></span> How does it work?
  </button>
  <div id='myModal' class='modal'>
  </div>
<div id='amazon-frame'>
<form method='post' action='amazon?update=post'>
  <div class='form-group'>
  <label for='usa'>America</label>
  <input id='usa' name='usa' type='text' aria-describedby='usaHelpBlock' required='required' class='form-control' style='width: 300px;' value=<?php echo checkonelink($conn,$country = 'usa');?> readonly>
  </div>
  <div class='form-group'>
  <label for='gbr'>UK</label>
  <input id='gbr' name='gbr' type='text' aria-describedby='gbrHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7' value=<?php echo checkonelink($conn,$country = 'gbr');?>>
  </div>
  <div class='form-group'>
  <label for='deu'>Germany</label>
  <input id='deu' name='deu' type='text' aria-describedby='deuHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7' value=<?php echo checkonelink($conn,$country = 'deu');?>>
  </div>
  <div class='form-group'>
  <label for='can'>Canada</label>
  <input id='can' name='can' type='text' aria-describedby='canHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7' value=<?php echo checkonelink($conn,$country = 'can');?>>
  </div>
  <div class='form-group'>
  <label for='esp'>Spain</label>
  <input id='esp' name='esp' type='text' aria-describedby='espHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7' value=<?php echo checkonelink($conn,$country = 'esp');?>>
  </div>
  <div class='form-group'>
  <label for='jpn'>Japan</label>
  <input id='jpn' name='jpn' type='text' aria-describedby='jpnHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7' value=<?php echo checkonelink($conn,$country = 'jpn');?>>
  </div>
  <div class='form-group'>
  <label for='fra'>France</label>
  <input id='fra' name='fra' type='text' aria-describedby='fraHelpBlock' class='form-control' style='width: 300px;' pattern='.{7}' maxlength='7' value=<?php echo checkonelink($conn,$country = 'fra');?>>
  </div>
  <div class='form-group'>
<button type='submit' class='btn btn-primary' name='update'>Update link</button>
</div>
</form>
</div>
</div>
</div>
