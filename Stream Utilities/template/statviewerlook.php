<?php
require_once "../../smart/config.ini.php";
require_once "../functions/class.security.php";
require_once "../functions/statviewer.php";
$security->checkvalid($conn);
if(!$security->checktwitch($conn)){
  echo "
        <h1>Stats of ".getname($conn,$client_id_admin)."</h1>
        <br>
        <h3 class='counter'>Views: ".statviewer($conn,$type = 'views',$client_id_admin)."<br>
        Followers: ".statviewer($conn,$type = 'followers',$client_id_admin)."<br>
        Current amount of Logged in Chatters: ".getchatters($conn,$client_id_admin)."<br>";
        if(getviewercount($conn,$client_id_admin)){
          echo "Current amount of viewers: ".getviewercount($conn,$client_id_admin)."<br>";
        }
          echo "</h3></span></div>";
}
?>
