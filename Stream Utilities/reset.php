<?php
include ("../smart/config.ini.php");
include ("functions/class.account.php");
$uniqueid = $_SESSION['UniqueId'];
if (isset($_POST[$uniqueid], $_POST['email'], $_POST['token'], $_POST['confirmpass'])) {
  $array = $account->resetpassword($conn, $recaptcha_secret);
  if(!empty($array['redirect'])){
    $redirect = $array['redirect'];
    $security->createUniqueId($generateUniqueId = true); //Resetting uniqueId
    header('location:'.$redirect);
    echo "<a href='".$redirect."'>Click here is you aren't redirected properly</a>";
  } else {
    echo '<div class="alert"><span class="closebtn">&times;</span><strong>Error!</strong><br>' . $array['text'] . '</div>';
  }
} elseif(!empty($_GET['key']) && !empty($_GET['reset'])) {
    $email = $_GET['key'];
    $token = $_GET['reset'];
    $records = $conn->prepare('SELECT email, token FROM  users WHERE email=:email AND token = :token');
    $records->bindParam(':email', $email);
    $records->bindParam(':token', $token);
    $execute = $records->execute();
    $row = $records->fetch(PDO::FETCH_ASSOC);
    if ($row['token'] != $token) {
      header("location:index?novalidtoken");
    }
}
?>

    <head>
      <link rel="apple-touch-icon-precomposed" sizes="57x57" href="web-img/favicon/apple-touch-icon-57x57.webp" />
      <link rel="icon" type="image/webp" href="web-img/favicon/streamutilities-32x32.webp" sizes="32x32" />
      <meta name="application-name" content="&nbsp;"/>
      <meta name="msapplication-TileColor" content="#FFFFFF" />
      <meta name="msapplication-TileImage" content="web-img/favicon/mstile-144x144.webp" />
      <title>Stream Utilities: Reset password</title>
      <script src="https://www.google.com/recaptcha/api.js?render=6LcfObcUAAAAABVww4LbZOXXB83forSKFevJ1W59"></script>
      <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <script async custom-element="amp-auto-ads"
             src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
     </script>
     <style>
     .alert {
     	padding: 15px;
     	background-color: #ee1111;
     	color: white;
     	opacity: 1;
     	transition: opacity 0.6s;
     	margin-bottom: 15px;
     	width: 25%;
     	margin: auto;
     	text-align: center;
     	border-radius: .25rem;
     }
     </style>
    </head>
    <div id="reset-wrapper">
        <div id="reset-head">
            <h4 style="padding-left:16px;">Reset password</h4>
        </div>
        <div id="reset-frame">
            <div id="reset">
                <form method="post">
                    <input type="hidden" name="email" value="<?php echo $email; ?>">
                    <input type="hidden" name="token" value="<?php echo $token; ?>"> Put in a new password.
                    <br>
                    <br>
                    <div class="form-group">
          				    <label for="InputPassword">Password</label>
          				    <input type="password" class="form-control" id="InputPassword" placeholder="Enter password" name="<?php echo $_SESSION['UniqueId'] ?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" autocomplete="off" required>
          						<span toggle="#InputPassword" class="fa fa-fw fa-eye field-icon toggle-password"></span>
          						<p hidden id="password-strength-text"></p>
          					</div>
          				  <div class="form-group">
          				    <label for="InputPassword2">Confirm Password</label>
          				    <input type="password" class="form-control" id="InputPassword2" placeholder="Enter password again" name="confirmpass" autocomplete="off" required>
          				  </div>
										<button type="submit" class="btn btn-primary" id="reset-password" name="submit_password">Reset Password</button>
                    <input type="hidden" id="recaptchaResponse" name="recaptcha_response">
                </form>
            </div>
        </div>
    </div>
    </div>
    <script>

        var strength = {
            0: "Come on even a kid could guess this...",
            1: "Getting better but can be loads stronger.",
            2: "I know you can make up a better password.",
            3: "Nearly there just some more characters.",
            4: "Holy smokes this is a strong password :O"
        }
        var password = document.getElementById('password');
        var text = document.getElementById('password-strength-text');
        if (password) {
            password.addEventListener('input', function() {
                var val = password.value;
                var result = zxcvbn(val);

                // Update the password strength meter
                // Update the text indicator
                if (val !== "") {
                    $('#password-strength-text').fadeIn('fast');
                    $("p").next("br").remove();
                    $("p").next("br").remove();
                    text.innerHTML = "Password strength: <br>" + strength[result.score] + "<br><br>Your password must atleast have one number, one uppercase letter and have atleast 8 characters in it! ";
                } else {
                    text.innerHTML = "";
                }
            })
        };
$(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

grecaptcha.ready(function () {
    grecaptcha.execute('6LcfObcUAAAAABVww4LbZOXXB83forSKFevJ1W59', { action: 'Login' }).then(function (token) {
        var recaptchaResponse = document.getElementById('recaptchaResponse');
        recaptchaResponse.value = token;
    });	});

    </script>
    <?php
    include('footer.php');
    ?>
